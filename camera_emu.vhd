--------------------------------------------------------------------------------
-- Company:        -
-- Engineer:       ko
-- 
-- Create Date:    14:56:40 07/10/2022 
-- Design Name:    Very simple OV7670 camera emulator (IP Core) module
-- Module Name:    ./camera_emu.vhd
-- Project Name:   ov7670_camera_emulator
-- Target Devices: Synthesizable code
-- Tool versions:  Xilinx ISE 14.7
-- Description:    This module generate static image.
--                 Emulated behaviour of camera ov7670 device: VGA/30fps/RGB565.
--
-- Dependencies: 
--  * files:
--  * modules:
--
-- Revision:
--  * Revision 0.01 - File Created
--    - Files: -
--    - Modules: -
--    - Processes:
--      - Architecture: Behavioral
--        - p0 - check the clock period
--        - p1 - generate VSYNC sync pulse
--        - p2 - generate HREF pulse on falling edge PCLK
--        - p3 - generate 'random' data on D7-D0
--
-- Concepts/Milestones:
-- (...)
--
-- Imporant signals/variables/constants:
-- (...)
--
-- Information from the software vendor:
--  * Messeges:
--  * Bugs:
--  * Notices:
--  * Infos:
--  * Notes:
--  * Criticals/Failures:
--
-- Additional Comments:
--  * All behavioral is get from PDF datasheet (see commmented).
--  * For display properly image, use RAW_RGB=1 and XCLK to 48 MHz.
--    See Generic settings in Entity.
--  * Timings based on datasheet VGA Frame Timing Figure 6 page 7.
--  * Entity signal names is on page 1.
--  * Device sends data on the falling edge, processed to his clock.
--  * Generic parameters have:
--    - CLOCK_PERIOD
--      Min / Typ / Max Unit
--      10  / 24  / 48  MHZ
--      100 / 42  / 21  ns
--    - RAW_RGB
--      0 - RAW (8 bit)
--      1 - RGB (16 bit)
--    - ZERO - not used
--  * Datasheet information:
--    - page 14 - 15 COM10 0x00 RW [2] - VSYNC changes on falling edge PCLK.
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity camera_emu is
generic (
constant CLOCK_PERIOD : integer := 21;
constant RAW_RGB      : integer := 1;
constant ZERO         : boolean := false
);
port (
SIO_C                 : inout std_logic;
SIO_D                 : inout std_logic;
VSYNC                 : out std_logic;
HREF                  : out std_logic;
PCLK                  : out std_logic;
D0                    : out std_logic;
D1                    : out std_logic;
D2                    : out std_logic;
D3                    : out std_logic;
D4                    : out std_logic;
D5                    : out std_logic;
D6                    : out std_logic;
D7                    : out std_logic;
XCLK                  : in std_logic;
RESETb                : in std_logic;
PWDN                  : in std_logic
);
end camera_emu;

architecture Behavioral of camera_emu is
  -- Constants
  constant CLOCK_PERIOD1 : integer := 21;
  constant CLOCK_PERIOD2 : integer := 42;
  constant CLOCK_PERIOD3 : integer := 100;
  -- 1 or 2 pclk for tp
  constant tp : integer := 2**RAW_RGB;
  -- tline = 784tp = 640tp + 144tp
  constant HREF0   : integer := 144;
  constant HREF1   : integer := 640;
  constant CHREF0  : integer := HREF0*tp; -- HREF 0 pulse time
  constant CHREF1  : integer := HREF1*tp; -- HREF 1 pulse time
  constant tline   : integer := CHREF1+CHREF0;
  -- VSYNC pulse have 510tline
  constant CVSYNC1 : integer := 3;
  constant CVSYNC2 : integer := 17;
  constant CVSYNC3 : integer := 480;
  constant CVSYNC4 : integer := 10;
  constant CVSYNCALL : integer := CVSYNC1+CVSYNC2+CVSYNC3+CVSYNC4; -- 510tline
  constant VSYNC1  : integer := CVSYNC1*tline;
  constant VSYNC2  : integer := CVSYNC2*tline;
  constant VSYNC3  : integer := CVSYNC3*tline;
  constant VSYNC4  : integer := CVSYNC4*tline;
  -- Signals
  signal href_time,pixel_time : std_logic;
begin
  PCLK <= XCLK; -- assume we return the same clock

	p0 : process (RESETb) is
	begin
		if (RESETb = '0') then
			assert (ZERO = ZERO
			  or CLOCK_PERIOD = CLOCK_PERIOD1
			  or CLOCK_PERIOD = CLOCK_PERIOD2
			  or CLOCK_PERIOD = CLOCK_PERIOD3)
			report "-- CLOCK_PERIOD must have " &
			        integer'image(CLOCK_PERIOD1) & "," &
			        integer'image(CLOCK_PERIOD2) & "," &
			        integer'image(CLOCK_PERIOD3) & " --"
			severity failure;
		end if;
	end process p0;

	p1 : process (XCLK,RESETb) is
    constant VBIT   : std_logic := '0'; -- toggle VSYNC camera
		type states is (svs1,svs2,svs3,svs4);
		variable state  : states;
    variable count  : integer range 0 to CVSYNCALL*tline-1;
		variable vvsync : std_logic;
	begin
		if (RESETb = '0') then
			count := 0;
			vvsync := VBIT;
			state := svs1;
			href_time <= '0';
		elsif (falling_edge(XCLK)) then
			case (state) is
				when svs1 =>
					vvsync := not VBIT;
					href_time <= '0';
					if (count = VSYNC1) then
						state := svs2;
						count := 0;
					else
						state := svs1;
						count := count + 1;
					end if;
				when svs2 =>
					vvsync := VBIT;
					href_time <= '0';
					if (count = VSYNC2) then
						state := svs3;
						count := 0;
					else
						state := svs2;
						count := count + 1;
					end if;
				when svs3 =>
					vvsync := VBIT;
					href_time <= '1';
					if (count = VSYNC3) then
						state := svs4;
						count := 0;
					else
						state := svs3;
						count := count + 1;
					end if;
				when svs4 =>
					vvsync := VBIT;
					href_time <= '0';
					if (count = VSYNC4) then
						state := svs1;
						count := 0;
					else
						state := svs4;
						count := count + 1;
					end if;
			end case;
			VSYNC <= vvsync;
		end if;
	end process p1;

	p2 : process (RESETb,XCLK,href_time) is
		type states is (swait4vsync,shref1,shref0);
		variable state   : states;
		variable count   : integer range 0 to VSYNC3-1;
		variable counth1 : integer range 0 to CHREF1-1;
		variable counth0 : integer range 0 to CHREF0-1;
		variable vhref   : std_logic;
	begin
		if (RESETb = '0') then
			count := 0;
			counth1 := 0;
			counth0 := 0;
			state := swait4vsync;
			vhref := '0';
		elsif (falling_edge(XCLK)) then
			case (state) is
				when swait4vsync =>
					pixel_time <= '0';
					if (href_time = '1') then
						if (count = VSYNC3) then
							state := swait4vsync;
							count := 0;
						else
							state := shref1;
							pixel_time <= '1';
							count := count + 1;
						end if;
					else
						state := swait4vsync;
					end if;
				when shref1 =>
					pixel_time <= '1';
					vhref := '1';
					if (counth1 = CHREF1 - 1) then
						pixel_time <= '0';
						state := shref0;
						counth1 := 0;
					else
						state := shref1;
						counth1 := counth1 + 1;
					end if;
				when shref0 =>
					pixel_time <= '0';
					vhref := '0';
					if (counth0 = CHREF0 - 1) then
						state := swait4vsync;
						counth0 := 0;
					else
						state := shref0;
						counth0 := counth0 + 1;
					end if;
			end case;
			HREF <= vhref;
		end if;
	end process p2;

	p3 : process (RESETb,XCLK,pixel_time) is
		constant CDATALENGTH : integer := 5;
		constant CNUMPIXELS  : integer := HREF1-CDATALENGTH*2;
		type tdata is array(0 to CDATALENGTH - 1) of std_logic_vector(7 downto 0);
		type states is (s1,s2,s3);
		variable state     : states;
		variable count     : integer range 0 to CDATALENGTH-1;
		constant startdata : tdata := (x"FF",x"EE",x"DD",x"CC",x"BB");
		constant enddata   : tdata := (x"BB",x"CC",x"DD",x"EE",x"FF");
		constant odddata   : std_logic_vector(7 downto 0) := x"AA";
		constant evendata  : std_logic_vector(7 downto 0) := x"55";
		variable vd        : std_logic_vector(7 downto 0);
	begin
		if (RESETb = '0') then
			vd := (others => '0');
			state := s1;
			count := 0;
		elsif (falling_edge(XCLK)) then
			if (pixel_time = '1') then
				case (state) is
					when s1 =>
						vd := startdata(count);
						if (count = CDATALENGTH - 1) then
							count := 0;
							state := s2;
						else
							count := count + 1;
							state := s1;
						end if;
					when s2 =>
						if (count = CNUMPIXELS - 1) then
							state := s3;
							count := 0;
						else
							state := s2;
							if (count mod 2 = 0) then
								vd := odddata;
								count := count + 1;
							elsif (count mod 2 = 1) then
								vd := evendata;
								count := count + 1;
							else
								vd := (others => 'U');
							end if;
						end if;
					when s3 =>
						vd := enddata(count);
						if (count = CDATALENGTH - 1) then
							count := 0;
							state := s1;
						else
							count := count + 1;
							state := s3;
						end if;
				end case;
			else
				vd := (others => '0');
			end if;
      D7 <= vd (7);
      D6 <= vd (6);
      D5 <= vd (5);
      D4 <= vd (4);
      D3 <= vd (3);
      D2 <= vd (2);
      D1 <= vd (1);
      D0 <= vd (0);
		end if;
	end process p3;

end Behavioral;
