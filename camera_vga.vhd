-------------------------------------------------------------------------------
-- Company:        -
-- Engineer:       ko
--
-- Create Date:    14:56:40 07/10/2022
-- Design Name:    Very simple OV7670 camera emulator (IP Core) module
-- Module Name:    ./camera_vga.vhd
-- Project Name:   ov7670_camera_emulator
-- Target Devices: Non-Synthesizable code
-- Tool versions:  Xilinx ISE 14.7
-- Description:    This module generate one frame 640x480/RGB565 from ROM.
--
-- Dependencies:
--  - Files:
--    - p_constants.vhd
--  - Modules:
--
-- Revision:
--  - Revision 0.01 - File Created
--    - Files: -
--    - Modules: -
--    - Processes (Architecture: behavioral_one_frame):
--        - p_reset           - check the clock period
--        - p_vsync           - gen vsync sync pulse
--        - p_href            - gen href pulse on falling edge pclk
--        - p_data_one_frame  - gen 'random' data on D7-D0
--  - Revision 0.02
--    - Files:
--      - p_constants.vhd
--    - Modules: -
--    - Processes (Architecture: behavioral):
--        - p_data  - gen data on D7-D0 from ROM (captured_frame_mem object)
--  - Revision 0.03
--    - Files: -
--    - Modules: -
--    - Processes (Architecture: behavioral):
--        - p_sccb_data - get data from SCCB Master (write)
--          - DEVICE_ID
--          - Address
--          - Data
--
-- Concepts/Milestones:
-- (...)
--
-- Imporant signals/variables/constants:
-- (...)
--
-- Information from the software vendor:
--  - Messeges:
--  - Bugs:
--  - Notices:
--    - Strange behaviour - 'X's on D7-D0 when assign outside p_data
--  - Infos:
--  - Notes:
--  - Criticals/Failures:
--
-- Additional Comments:
--  - All behavioral is get from PDF datasheet (see commmented).
--  - For display properly image, use raw_rgb=1 and xclk to 48 MHz.
--  - Timings based on datasheet VGA Frame Timing Figure 6 page 7.
--  - Entity signal names is on page 1.
--  - Device sends data on the falling edge, processed to his clock.
--  - All time assume return the same period like clock input.
--  - Generic parameters have:
--    - clock_period
--      Min / Typ / Max Unit
--      10  / 24  / 48  MHZ
--      100 / 42  / 21  ns
--    - raw_rgb
--      0 - RAW (8 bit)
--      1 - RGB (16 bit)
--    - zero - not used
--  - Datasheet information:
--    - page 14 (Table 5. Device Control Register List).
--      -15 COM10 0x00 RW [2] - VSYNC changes on falling edge pclk.
--      15 COM10 0x00 RW [1] - VSYNC negative ('camera_vs' signal in p_vsync).
--    - page 7 (Figure 6) - some timings based on diagram.
--      Tp is 1 or 2 PCLK depend on RAW or YUV/RGB data mode (see note).
--        HREF0 have 144 Tp's.
--        HREF1 have 640 Tp's.
--          Tline have sum of HREF0 and HREF1.
--            VSYNC Pulse have 3 Tline's.
--            VSYNC Front Porch 17 Tline's.
--            HREF data row have 480 Tline's.
--            VSYNC Back Porch is 10 Tline's.
--              VSYNC period have 510 Tline's.
--      TODO: XXX HSYNC supplement.
--
-------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use work.p_constants.all;

entity camera_vga is
  generic (
    constant clock_period : integer := 21;
    constant raw_rgb      : integer := 1;
    constant zero         : boolean := false
  );
  port (
    sio_c   : in    std_logic;
    sio_d   : inout std_logic;
    vsync   : out   std_logic;
    href    : out   std_logic;
    pclk    : out   std_logic;
    d0      : out   std_logic;
    d1      : out   std_logic;
    d2      : out   std_logic;
    d3      : out   std_logic;
    d4      : out   std_logic;
    d5      : out   std_logic;
    d6      : out   std_logic;
    d7      : out   std_logic;
    xclk    : in    std_logic;
    reset_n : in    std_logic;
    pwdn    : in    std_logic
  );
end entity camera_vga;

architecture behavioral_one_frame of camera_vga is

  -- Constants
  constant clock_period1 : integer := 21;
  constant clock_period2 : integer := 42;
  constant clock_period3 : integer := 100;
  constant tp            : integer := 2 ** raw_rgb;
  constant href0         : integer := 144;
  constant href1         : integer := 640;
  constant chref0        : integer := href0 * tp;
  constant chref1        : integer := href1 * tp;
  constant tline         : integer := chref1 + chref0;
  constant cvsync1       : integer := 3;
  constant cvsync2       : integer := 17;
  constant cvsync3       : integer := 480;
  constant cvsync4       : integer := 10;
  constant cvsyncall     : integer := cvsync1 + cvsync2 + cvsync3 + cvsync4;
  constant vsync1        : integer := cvsync1 * tline;
  constant vsync2        : integer := cvsync2 * tline;
  constant vsync3        : integer := cvsync3 * tline;
  constant vsync4        : integer := cvsync4 * tline;
  signal pixel_time : std_logic;

begin

  -- p_reset : process...
  -- p_vsync : process...
  -- p_href  : process...
  p_data_one_frame : process (xclk, reset_n) is
    constant CDATALENGTH : integer := 5;
    constant CNUMPIXELS  : integer := HREF1 - CDATALENGTH * 2;
    type tdata is array (0 to CDATALENGTH - 1) of std_logic_vector(7 downto 0);
    type states is (s1,s2,s3);
    -- FFs is exacly white screen
    -- constant startdata : tdata := (x"FF",x"FF",x"FF",x"FF",x"FF");
    -- constant enddata   : tdata := (x"FF",x"FF",x"FF",x"FF",x"FF");
    -- constant odddata   : std_logic_vector(7 downto 0) := x"FF";
    -- constant evendata  : std_logic_vector(7 downto 0) := x"FF";
    -- 'Random' data with start/end patterns
    constant startdata : tdata := (x"FF",x"EE",x"DD",x"CC",x"BB");
    constant enddata   : tdata := (x"BB",x"CC",x"DD",x"EE",x"FF");
    constant odddata   : std_logic_vector(7 downto 0) := x"55";
    constant evendata  : std_logic_vector(7 downto 0) := x"55";
    variable state : states;
    variable count : integer range 0 to CDATALENGTH - 1;
    variable vd    : std_logic_vector(7 downto 0);
  begin
    if (reset_n = '0') then
      vd := (others => '0');
      state := s1;
      count := 0;
    elsif (falling_edge (xclk)) then
      if (pixel_time = '1') then
        case (state) is
          when s1 =>
            vd := startdata(count);
            if (count = CDATALENGTH - 1) then
              count := 0;
              state := s2;
            else
              count := count + 1;
              state := s1;
            end if;
          when s2 =>
            if (count = CNUMPIXELS - 1) then
              state := s3;
              count := 0;
            else
              state := s2;
              if (count mod 2 = 0) then
                vd := odddata;
                count := count + 1;
              elsif (count mod 2 = 1) then
                vd := evendata;
                count := count + 1;
              else
                vd := (others => 'U');
              end if;
            end if;
          when s3 =>
            vd := enddata(count);
            if (count = CDATALENGTH - 1) then
              count := 0;
              state := s1;
            else
              count := count + 1;
              state := s3;
            end if;
        end case;
      else
        vd := (others => '0');
      end if;
      d7 <= vd (7);
      d6 <= vd (6);
      d5 <= vd (5);
      d4 <= vd (4);
      d3 <= vd (3);
      d2 <= vd (2);
      d1 <= vd (1);
      d0 <= vd (0);
    end if;
  end process p_data_one_frame;

end architecture behavioral_one_frame;

architecture behavioral of camera_vga is

  -- Constants
  constant clock_period1 : integer := 21;
  constant clock_period2 : integer := 42;
  constant clock_period3 : integer := 100;
  constant tp            : integer := 2 ** raw_rgb;
  constant href0         : integer := 144;
  constant href1         : integer := 640;
  constant chref0        : integer := href0 * tp;
  constant chref1        : integer := href1 * tp;
  constant tline         : integer := chref1 + chref0;
  constant cvsync1       : integer := 3;
  constant cvsync2       : integer := 17;
  constant cvsync3       : integer := 480;
  constant cvsync4       : integer := 10;
  constant cvsyncall     : integer := cvsync1 + cvsync2 + cvsync3 + cvsync4;
  constant vsync1        : integer := cvsync1 * tline;
  constant vsync2        : integer := cvsync2 * tline;
  constant vsync3        : integer := cvsync3 * tline;
  constant vsync4        : integer := cvsync4 * tline;
  -- Signals
  signal camera_vs         : std_logic;
  signal href_time         : std_logic;
  signal pixel_time        : std_logic;
  constant c_sccb_data_len : integer := 24;
  signal sccb_data         : std_logic_vector(c_sccb_data_len - 1 downto 0);
  signal sccb_data_sr      : std_logic_vector(c_sccb_data_len - 1 downto 0);
  signal sccb_sio_d_i      : std_logic;

begin

  sccb_sio_d_i <= sio_d;

  p_sccb_data : process (sio_c, reset_n, pwdn) is
    constant c_sio_d_index : integer := 25;
    variable v_sio_d_index : integer range 0 to c_sio_d_index - 1;
  begin
    if (reset_n = '0') then
      sccb_data_sr <= (others => '0');
      sio_d <= '0';
      v_sio_d_index := 0;
      sccb_data <= (others => '0');
    elsif (pwdn = '1') then
      sio_d <= 'Z';
      v_sio_d_index := 0;
      sccb_data <= (others => '0');
    elsif (rising_edge (sio_c)) then
      if (sccb_sio_d_i /= 'Z') then
        sccb_data_sr <= sccb_data_sr (sccb_data_sr'left - 1 downto 0) & sccb_sio_d_i;
        if (v_sio_d_index = c_sio_d_index - 1) then
          sccb_data <= sccb_data_sr;
          v_sio_d_index := 0;
        else
          v_sio_d_index := v_sio_d_index + 1;
        end if;
      end if;
    end if;
  end process p_sccb_data;

  pclk  <= xclk;
  vsync <= camera_vs;

  p_reset : process (reset_n) is
  begin

    if (reset_n = '0') then
      assert (zero = zero
        or clock_period = clock_period1
        or clock_period = clock_period2
        or clock_period = clock_period3)
        report "-- clock_period must have " &
               integer'image(clock_period1) & "," &
               integer'image(clock_period2) & "," &
               integer'image(clock_period3) & " --"
        severity failure;
    end if;

  end process p_reset;

  p_vsync : process (xclk, reset_n) is

    variable count  : integer range 0 to cvsyncall * tline - 1;
    variable vvsync : std_logic;

    type states is (svs1, svs2, svs3, svs4);

    variable state : states;

  begin

    if (reset_n = '0') then
      count     := 0;
      vvsync    := '1';
      state     := svs1;
      href_time <= '0';
    elsif (falling_edge(xclk)) then

      case (state) is

        when svs1 =>

          vvsync    := '0';
          href_time <= '0';
          if (count = vsync1 - 1) then
            state := svs2;
            count := 0;
          else
            state := svs1;
            count := count + 1;
          end if;

        when svs2 =>

          vvsync    := '1';
          href_time <= '0';
          if (count = vsync2 - 1) then
            state := svs3;
            count := 0;
          else
            state := svs2;
            count := count + 1;
          end if;

        when svs3 =>

          vvsync    := '1';
          href_time <= '1';
          if (count = vsync3 - 1) then
            state := svs4;
            count := 0;
          else
            state := svs3;
            count := count + 1;
          end if;

        when svs4 =>

          vvsync    := '1';
          href_time <= '0';
          if (count = vsync4 - 1) then
            state := svs1;
            count := 0;
          else
            state := svs4;
            count := count + 1;
          end if;

      end case;

      camera_vs <= not vvsync;
    end if;

  end process p_vsync;

  p_href : process (reset_n, xclk) is

    variable count   : integer range 0 to vsync3 - 1;
    variable counth1 : integer range 0 to chref1 - 1;
    variable counth0 : integer range 0 to chref0 - 1;

    type states is (swait4vsync, shref1, shref0);

    variable state : states;
    variable vhref : std_logic;

  begin

    if (reset_n = '0') then
      count   := 0;
      counth1 := 0;
      counth0 := 0;
      state   := swait4vsync;
      vhref   := '0';
    elsif (falling_edge(xclk)) then

      case (state) is

        when swait4vsync =>

          if (href_time = '1') then
            state      := shref1;
            pixel_time <= '1';
          else
            state      := swait4vsync;
            pixel_time <= '0';
          end if;

        when shref1 =>

          pixel_time <= '1';
          vhref      := '1';
          if (counth1 = chref1 - 1) then
            pixel_time <= '0';
            state      := shref0;
            counth1    := 0;
          else
            state   := shref1;
            counth1 := counth1 + 1;
          end if;

        when shref0 =>

          pixel_time <= '0';
          vhref      := '0';
          if (counth0 = chref0 - 1) then
            state   := swait4vsync;
            counth0 := 0;
          else
            state   := shref0;
            counth0 := counth0 + 1;
          end if;

      end case;

      href <= vhref;
    end if;

  end process p_href;

  p_data : process (xclk) is

    variable count : integer range 0 to C_CAMERA_FRAME_LENGTH - 1;
    variable vd    : std_logic_vector(7 downto 0);
    variable flag  : boolean;

  begin

    if (reset_n = '0') then
      vd    := (others => '0');
      count := 0;
      flag  := false;
    elsif (falling_edge(xclk)) then
      if (camera_vs = '1') then
        count := 0;
        flag  := false;
        vd    := (others => '0');
      end if;
      if (pixel_time = '1') then
        if (flag = false) then
          vd    := captured_frame_m (count)(15 downto 8);
          count := count;
          flag  := true;
        else
          vd    := captured_frame_m (count)(7 downto 0);
          count := count + 1;
          flag  := false;
        end if;
      else
        vd := (others => '0');
      end if;
      d7 <= vd (7);
      d6 <= vd (6);
      d5 <= vd (5);
      d4 <= vd (4);
      d3 <= vd (3);
      d2 <= vd (2);
      d1 <= vd (1);
      d0 <= vd (0);
    end if;

  end process p_data;

end architecture behavioral;
