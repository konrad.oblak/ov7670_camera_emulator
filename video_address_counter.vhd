-------------------------------------------------------------------------------
-- Company:       -
-- Engineer:      ko
--
-- Create Date:   12:38:33 05/09/2024
-- Design Name:   Very simple OV7670 camera emulator (IP Core) Testbench
-- Module Name:   video_address_counter - Behavioral
-- Project Name:  ov7670_camera_emulator
-- Target Device: Synthesizable code
-- Tool versions: Xilinx ISE 14.7
-- Description:   Used to addresses video out memory.
--
-- Denotes used in all code (only first/last prefix/suffix):
--  - c_   - constants
--  - g_   - generates
--  - i_   - input signals
--  - o_   - output signals
--  - p_   - processes
--  - v_   - variables
--  - io_  - inout signals
--  - lX_  - loops
--  - _i   - internal signals
--  - _iX  - instances
--  - _m   - memories
--  - _p   - packages
--  - _sr  - shift registers
--  - _t   - types
--  - _dut - Device Under Test
--  - _uut - Unit Under Test
--
-- Additional Comments: -
--
-- Dependencies:
--  - Files:
--    - numeric_std.vhd - version without debug enabled.
--  - Modules: -
--
-- Revision:
--  - Revision 0.01 - File created
--    - Files: -
--    - Modules: -
--    - Processes (Architecture: behavioral)
--      - p_video_address_counter - main process, address counter synchronized
--        with VGA timing for read data from PortB memory.
--
-- Concepts/Milestones: -
--
-- Imporant Subtypes/Types/Signals/Variables/Constants: -
--
-- Information from the software vendor:
--  - Messeges: -
--  - Bugs: -
--  - Notices: -
--  - Infos: -
--  - Notes: -
--  - Criticals/Failures: -
--
-------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;
  use work.numeric_std.all;

entity video_address_counter is
  generic (
    c_memory_data_bits   : integer := 0;
    c_memory_max_address : integer := 0
  );
  port (
    video_clock           : in    std_logic;
    video_reset           : in    std_logic;
    video_vsync           : in    std_logic;
    video_hsync           : in    std_logic;
    video_blank           : in    std_logic;
    video_address_counter : out   std_logic_vector(c_memory_data_bits - 1 downto 0)
  );
end entity video_address_counter;

architecture behavioral of video_address_counter is

  signal video_address_counter_i : std_logic_vector(c_memory_data_bits - 1 downto 0);

begin

  video_address_counter <= video_address_counter_i;

  p_video_address_counter : process (video_reset, video_clock) is
    begin

      if (video_reset = '1') then
        video_address_counter_i <= (others => '0');
      elsif (rising_edge (video_clock)) then
        if (video_vsync = '1') then
          video_address_counter_i <= (others => '0');
        elsif (video_hsync = '1') then
          if (conv_integer (video_address_counter_i) = c_memory_max_address - 1) then
            video_address_counter_i <= (others => '0');
          else
            if (video_blank = '1') then
              video_address_counter_i <= video_address_counter_i;
            else
              video_address_counter_i <= std_logic_vector(unsigned (video_address_counter_i) + 1);
            end if;
          end if;
        end if;
      end if;

  end process p_video_address_counter;

end architecture behavioral;
