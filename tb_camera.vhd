-------------------------------------------------------------------------------
-- Company:       -
-- Engineer:      ko
--
-- Create Date:   16:18:44 07/10/2022
-- Design Name:   Very simple OV7670 camera emulator (IP Core) Testbench
-- Module Name:   ./tb_ov7670_camera_emulator.txt
-- Project Name:  ov7670_camera_emulator
-- Target Device: Non-Synthesizable code
-- Tool versions: Xilinx ISE 14.7
-- Description:   This testbench use several modules connected together to
--                emulate OV7670 camera behavioral, and show flow demo,
--                how read RAW data from file and write to image. (...)
--
-- Additional Comments:
--  - Module is only for simulation.
--  - Main clock don't exists (eg. board), only clock's for video and camera.
--  - Module load *.hex data for simulation.
--    Each .hex file is RAW data storing one frame 640x480 with RGB888 color.
--    Exists 30 .hex files, so we have 1 second animation (camera max 30 fps).
--    On this time, only one frame is used:
--      - from file hex_memory_file_frame1.hex
--      - c_hex_rom_files_count = 1
--  - For properly reset VGA timings and don't have poorly image, 'video_reset'
--    must be hold equal-or-less than half 'video_clock_period'.
--  - To write stream to BMP file, 'video_blank' signal must be negative.
--  - Main stimulus process is 'stim_proc'.
--  - Main component to test (UUT) is 'camera_vga'.
--  - Wait for 4 frames from VGA signal, so BMP should be writen.
--  - Assume hold reset for 100 ns for camera.
--  - Currently used architecture is at end of VHD file
--
-- Dependencies:
--  - Files:
--    - numeric_std.vhd
--      copy of with commented lines 3211-3213 and 3181-3182
--      for boost ISIM simulation, but better is set flag NO_WARNING (line 883)
--    - p_constants.vhd
--      store camera default values, function and procedures (...)
--    - hex_memory_file_frame*.hex
--      files with RAW data for each frame - store RAW data for VGA 30 fps
--  - Modules:
--    - Bitmap-VHDL-Package
--      - git clone https://github.com/jherkenhoff/Bitmap-VHDL-Package.git
--      - use Bitmap-VHDL-Package/rtl/*.vhd files in project
--    - ov_sccb
--      - git clone https://github.com/csus-senior-design/ov_sccb.git
--      - use ov_sccb/*.v files in project
--
-- Revision:
--  - Revision 0.01 - File created (Original version)
--    - Files:
--      - camera_config_qqvga_rgb444.vhd
--      - camera_config_qqvga_yuv.vhd
--      - camera_vga.vhd
--      - tb_camera_qqvga.vhd
--    - Modules:
--      camera - main module (...)
--    - Processes:
--      stim_proc - main process, we wait one VGA frame (~16.8ms on ~25MHz)
--      camera_i_xclk_process - clock generator for camera module
--  - Revision 0.02
--    - Files:
--      - hex_memory_file_frame*.hex
--      - p_constants.vhd
--      - tb_test1.vhd
--    - Modules:
--      - vga_timing_synch - VGA timing
--      - vga_bmp_sink - write VGA frame to BMP
--    - Processes:
--      - load_memory_from_files - load RAW RGB888 data from one HEX file and
--        fill memory with frame converted to RGB565 data
--      - video_clock_process - clock generator for VGA timing
--  - Revision 0.03
--    - Files:
--      - memory_dualport.vhd
--      - video_address_counter.vhd
--      - camera_capture.vhd
--    - Modules:
--      - inst_dualmemory - implement Dual Port RAM (2xCLK,PortA write),
--      - video_address_counter_i1 - implement address counter for out video.
--      - camera_capture_i1 - logic for capture data from camera module.
--    - Processes: -
--  - Revision 0.04
--    - Files: -
--    - Modules: -
--    - Processes:
--      - p_v?_mux - multiplex video signals to current vga_bmp_sink component
--        generated in g_write_bmps
--      - p_write_bmps - increment component index on video vsync
--  - Revision 0.05 (4*):
--    - Files:
--      - sccb.vhd
--    - Modules:
--      - i2c_sender_i1 - implement Omnivision SCCB Master, with properly SIO_C
--        clock, only write to Slave Device
--    - Processes:
--      - p_sccb_service - example usage from ov_sccb_tb.v tb
--
-- Concepts/Milestones:
--  - emulate jittering in clock (1*) and exhaustive tests (2*)
--  - move raw data from captured frames to FPGA RAM block's (3*)
--  - implement SSCB with R/W settings to camera registers (4*)
--  - exhaustive documented modules (N...)
--
-- Imporant Subtypes/Types/Signals/Variables/Constants: -
--
-- Information from the software vendor:
--  - Messeges: -
--  - Bugs: -
--  - Notices: -
--  - Infos: -
--  - Notes: -
--  - Criticals/Failures: -
--
-------------------------------------------------------------------------------
-- * VHDL Test Bench Created by ISE for module: camera *
--
--  This testbench has been automatically generated using types std_logic and
--  std_logic_vector for the ports of the unit under test. Xilinx recommends
--  that these types always be used for the top-level I/O of a design in order
--  to guarantee that the testbench will bind correctly to the
--  post-implementation simulation model.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- Please keep the documentation up to date, clear and clean as possible     --
-------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use work.numeric_std.all;
  use work.p_constants.all;

entity tb_ov7670_camera_emulator is
end entity tb_ov7670_camera_emulator;

architecture behavioral of tb_ov7670_camera_emulator is

  component camera_vga is
    generic (
      constant clock_period : integer := 21;
      constant raw_rgb      : integer := 1;
      constant zero         : boolean := false
    );
    port (
      sio_c   : in    std_logic;
      sio_d   : inout std_logic;
      vsync   : out   std_logic;
      href    : out   std_logic;
      pclk    : out   std_logic;
      d0      : out   std_logic;
      d1      : out   std_logic;
      d2      : out   std_logic;
      d3      : out   std_logic;
      d4      : out   std_logic;
      d5      : out   std_logic;
      d6      : out   std_logic;
      d7      : out   std_logic;
      xclk    : in    std_logic;
      reset_n : in    std_logic;
      pwdn    : in    std_logic
    );
  end component camera_vga;
  for all : camera_vga use entity work.camera_vga (behavioral);

  component i2c_sender is
    port (
    clk   : in    std_logic;
    siod  : inout std_logic;
    sioc  : out   std_logic;
    taken : out   std_logic;
    send  : in    std_logic;
    id    : in    std_logic_vector(7 downto 0);
    reg   : in    std_logic_vector(7 downto 0);
    value : in    std_logic_vector(7 downto 0)
    );
  end component i2c_sender;

  component rama_12 is
    generic (
      constant addr_bits : integer := c_camera_frame_memory_data_bits;
      constant data_bits : integer := c_bits_color_rgb565
    );
    port (
      do1  : out   std_logic_vector(data_bits - 1 downto 0);
      do2  : out   std_logic_vector(data_bits - 1 downto 0);
      di   : in    std_logic_vector(data_bits - 1 downto 0);
      add1 : in    std_logic_vector(addr_bits - 1 downto 0);
      add2 : in    std_logic_vector(addr_bits - 1 downto 0);
      clk1 : in    std_logic;
      clk2 : in    std_logic;
      we   : in    std_logic
    );
  end component rama_12;

  component vga_bmp_sink is
    generic (
      filename : string
    );
    port (
      clk_i        : in    std_logic;
      active_vid_i : in    std_logic;
      h_sync_i     : in    std_logic;
      v_sync_i     : in    std_logic;
      dat_i        : in    std_logic_vector(23 downto 0)
    );
  end component vga_bmp_sink;

  component video_timing is
    port (
      reset           : in    std_logic;
      video_clock     : in    std_logic;
      video_hsync     : out   std_logic;
      video_vsync     : out   std_logic;
      video_blank     : out   std_logic;
      active_area     : out   std_logic;
      active_haddrgen : out   std_logic;
      active_render   : out   std_logic
    );
  end component video_timing;
  for all : video_timing use entity work.video_timing (behavioral_3);

  component video_address_counter is
    generic (
      c_memory_data_bits   : integer := c_camera_frame_memory_data_bits;
      c_memory_max_address : integer := c_camera_frame_length
    );
    port (
      video_clock           : in    std_logic;
      video_reset           : in    std_logic;
      video_vsync           : in    std_logic;
      video_hsync           : in    std_logic;
      video_blank           : in    std_logic;
      video_address_counter : out   std_logic_vector(c_memory_data_bits - 1 downto 0)
    );
  end component video_address_counter;

  component camera_capture is
    port (
      i_camera_rst                  : in    std_logic;
      i_camera_pclk                 : in    std_logic;
      i_camera_vsync                : in    std_logic;
      i_camera_href                 : in    std_logic;
      i_camera_data                 : in    std_logic_vector(7 downto 0);
      o_camera_data                 : out   std_logic_vector(15 downto 0);
      o_camera_address_write_enable : out   std_logic;
      o_camera_address_counter      : out   std_logic_vector(c_camera_frame_memory_data_bits - 1 downto 0)
    );
  end component camera_capture;

  constant camera_i_xclk_period : time := 21 ns;
  constant video_clock_period   : time := 39.80099502487 ns; -- industrial clock 25.175MHz
  -- constant video_clock_period   : time := 40 ns; -- normal clock 25Mhz

  signal camera_io_scl : std_logic;
  signal camera_io_sda : std_logic;
  signal camera_i_xclk : std_logic;
  signal camera_i_rst  : std_logic;
  signal camera_i_pwdn : std_logic;
  signal camera_o_vs   : std_logic;
  signal camera_o_hs   : std_logic;
  signal camera_o_hs_q : std_logic;
  signal camera_o_pclk : std_logic;
  signal camera_o_d    : std_logic_vector(7 downto 0);

  signal camera_address_counter      : std_logic_vector(c_camera_frame_memory_data_bits - 1 downto 0);
  signal camera_data                 : std_logic_vector(c_bits_color_rgb565 - 1 downto 0);
  signal camera_address_write_enable : std_logic;
  signal video_address_counter_i     : std_logic_vector(c_camera_frame_memory_data_bits - 1 downto 0);
  signal video_data                  : std_logic_vector(c_bits_color_rgb565 - 1 downto 0);
  signal video_reset                 : std_logic;
  signal video_clock                 : std_logic;
  signal video_blank                 : std_logic;
  signal video_hsync                 : std_logic;
  signal video_vsync                 : std_logic;

  constant number_frames_to_catch    : integer := 11;
  signal number_frame                : integer := 0;
  signal video_clock_mux             : std_logic_vector(number_frames_to_catch - 1 downto 0);
  signal video_blank_mux             : std_logic_vector(number_frames_to_catch - 1 downto 0);
  signal video_hsync_mux             : std_logic_vector(number_frames_to_catch - 1 downto 0);
  signal video_vsync_mux             : std_logic_vector(number_frames_to_catch - 1 downto 0);

  signal sccb_clk     : std_logic;                     -- Clock signal
  signal sccb_sio_d   : std_logic;                     -- SCCB data (tri-state)
  signal sccb_sio_c   : std_logic;                     -- SCCB clock
  signal sccb_addr    : std_logic_vector(7 downto 0);  -- Address of device
  signal sccb_subaddr : std_logic_vector(7 downto 0);  -- Sub-Address (Register) to write to
  signal sccb_w_data  : std_logic_vector(7 downto 0);  -- Data to write to device
  signal sccb_done    : std_logic;
  signal sccb_wd_flag : std_logic;

begin

  sccb_clk <= camera_i_xclk;
  camera_io_scl <= sccb_sio_c;
  camera_io_sda <= sccb_sio_d when sccb_wd_flag = '1' else 'Z';
  camera_i_pwdn <= not camera_i_rst;

  stim_proc : process is
  begin

    video_reset  <= '1';
    wait for video_clock_period / 2;
    video_reset  <= '0';
    camera_i_rst <= '0';
    wait for 100 ns;
    camera_i_rst <= '1';
    wait for camera_i_xclk_period * 10;
    -- insert stimulus here
    wait for number_frames_to_catch * 16.8 ms;
    report "tb done"
      severity failure;

  end process stim_proc;

  p_vc_mux : process (video_clock, number_frame) is
  begin
    video_clock_mux (number_frame) <= video_clock;
  end process p_vc_mux;

  p_vb_mux : process (video_blank, number_frame) is
  begin
    video_blank_mux (number_frame) <= video_blank;
  end process p_vb_mux;

  p_vh_mux : process (video_hsync, number_frame) is
  begin
    video_hsync_mux (number_frame) <= video_hsync;
  end process p_vh_mux;

  p_vv_mux : process (video_vsync, number_frame) is
  begin
    video_vsync_mux (number_frame) <= video_vsync;
  end process p_vv_mux;

  p_write_bmps : process is
  begin
    wait until video_vsync = '1';
    wait until video_vsync = '0';
    number_frame <= number_frame + 1;
  end process p_write_bmps;

  g_write_bmps : for number_frame in 1 to number_frames_to_catch - 1 generate
    vga_bmp : component vga_bmp_sink
      generic map (
        filename => "vga" & integer'image (number_frame) & ".bmp"
      )
      port map (
        clk_i        => video_clock_mux (number_frame),
        dat_i        => video_data (4 downto 0)  &"000"&
                        video_data (10 downto 5) &"00" &
                        video_data (15 downto 11)&"000",
        active_vid_i => not video_blank_mux (number_frame),
        h_sync_i     => video_hsync_mux (number_frame),
        v_sync_i     => video_vsync_mux (number_frame)
      );
  end generate g_write_bmps;

  camera_vga_uut : component camera_vga
    port map (
      sio_c   => camera_io_scl,
      sio_d   => camera_io_sda,
      vsync   => camera_o_vs,
      href    => camera_o_hs,
      pclk    => camera_o_pclk,
      d0      => camera_o_d (0),
      d1      => camera_o_d (1),
      d2      => camera_o_d (2),
      d3      => camera_o_d (3),
      d4      => camera_o_d (4),
      d5      => camera_o_d (5),
      d6      => camera_o_d (6),
      d7      => camera_o_d (7),
      xclk    => camera_i_xclk,
      reset_n => camera_i_rst,
      pwdn    => camera_i_pwdn
    );

  i2c_sender_i1 : component i2c_sender
    port map (
      clk   => sccb_clk,
      taken => sccb_done,
      siod  => sccb_sio_d,
      sioc  => sccb_sio_c,
      send  => sccb_wd_flag,
      id    => sccb_addr,
      reg   => sccb_subaddr,
      value => sccb_w_data
   );

  video_timing_i1 : component video_timing
    port map (
      reset           => video_reset,
      video_clock     => video_clock,
      video_hsync     => video_hsync,
      video_vsync     => video_vsync,
      video_blank     => video_blank,
      active_area     => open,
      active_haddrgen => open,
      active_render   => open
    );

  video_address_counter_i1 : component video_address_counter
  port map (
    video_clock           => video_clock,
    video_reset           => video_reset,
    video_vsync           => video_vsync,
    video_hsync           => video_hsync,
    video_blank           => video_blank,
    video_address_counter => video_address_counter_i
  );

  inst_dualmemory : component rama_12
    port map (
      clk1 => camera_o_pclk,
      we   => camera_address_write_enable,
      add1 => camera_address_counter,
      di   => camera_data,
      do1  => open,
      clk2 => video_clock,
      add2 => video_address_counter_i,
      do2  => video_data
    );

  camera_capture_i1 : component camera_capture
    port map (
      i_camera_rst                  => camera_i_rst,
      i_camera_pclk                 => camera_o_pclk,
      i_camera_vsync                => camera_o_vs,
      i_camera_href                 => camera_o_hs,
      i_camera_data                 => camera_o_d,
      o_camera_data                 => camera_data,
      o_camera_address_write_enable => camera_address_write_enable,
      o_camera_address_counter      => camera_address_counter
    );

  camera_i_xclk_process : process is
  begin

    camera_i_xclk <= '0';
    wait for camera_i_xclk_period / 2;
    camera_i_xclk <= '1';
    wait for camera_i_xclk_period / 2;

  end process camera_i_xclk_process;

  video_clock_process : process is
  begin

    video_clock <= '0';
    wait for video_clock_period / 2;
    video_clock <= '1';
    wait for video_clock_period / 2;

  end process video_clock_process;

  load_memory_from_files : process is

    variable start_addr : integer;
    variable file_name  : string (1 to 26);

  begin

    for i in 1 to c_hex_rom_files_count loop

      start_addr := 0;
      file_name  := c_hex_rom_files_name & integer'image(i) & "." & c_hex_rom_files_ext;
      report "readandconvertrom " & file_name;
      readandconvertrom(file_name, start_addr);

    end loop;

    wait;

  end process load_memory_from_files;

  p_sccb_service : process is
    constant CHIP_ADDR : std_logic_vector(7 downto 0) := x"42";
    procedure write_sccb (
      t_chip_addr : in std_logic_vector(7 downto 0);
      t_sub_addr : in std_logic_vector(7 downto 0);
      t_data : in std_logic_vector(7 downto 0)
    ) is
    begin
      report ("Writing " & hex (t_data) & " to register " & hex (t_sub_addr) & " on chip " & hex (t_chip_addr) & ")");
      wait until camera_i_xclk = '1';
      sccb_addr <= t_chip_addr;
      sccb_subaddr <= t_sub_addr;
      sccb_w_data <= t_data;
      wait until camera_i_xclk = '1';
      wait until camera_i_xclk = '1';
      wait until camera_i_xclk = '1';
      wait until camera_i_xclk = '1';
    end procedure write_sccb;
  begin
    sccb_wd_flag <= '0';
    write_sccb (CHIP_ADDR, "XXXXXXXX", "XXXXXXXX"); -- artificial wait
    wait for 200 us;
    report ("p_sccb_service Beginning write transactions");
    sccb_wd_flag <= '1';
    write_sccb (CHIP_ADDR, x"00", x"00"); -- some propagation before taken
    wait until sccb_done = '0';
    sccb_wd_flag <= '0';
    wait for camera_i_xclk_period;
    sccb_wd_flag <= '1';
    write_sccb (CHIP_ADDR, x"00", x"CA");
    wait until sccb_done = '0';
    sccb_wd_flag <= '0';
    wait for camera_i_xclk_period;
    sccb_wd_flag <= '1';
    write_sccb (CHIP_ADDR, x"0A", x"FE");
    wait until sccb_done = '0';
    sccb_wd_flag <= '0';
    wait for camera_i_xclk_period;
    sccb_wd_flag <= '1';
    write_sccb (CHIP_ADDR, x"10", x"D0");
    wait until sccb_done = '0';
    sccb_wd_flag <= '0';
    wait for camera_i_xclk_period;
    sccb_wd_flag <= '1';
    write_sccb (CHIP_ADDR, x"1A", x"BA");
    wait until sccb_done = '0';
    wait for camera_i_xclk_period;
    sccb_wd_flag <= '1';
    wait for camera_i_xclk_period;
    wait until sccb_done = '0';
    wait for camera_i_xclk_period;
    sccb_wd_flag <= '0';
    wait for camera_i_xclk_period;
    report "p_sccb_service done";
    wait;
    -- report "tb done" severity failure;
  end process p_sccb_service;

end architecture behavioral;
