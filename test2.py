#!/usr/bin/env python3

import cv2
import numpy as np
import imageio

video_path = "hw-vga.gif"
output_video_path = "output-vga.gif"
dest_fps = 30
image_count = 0
frames = []

video_capture = cv2.VideoCapture(video_path)

frame_width = int(video_capture.get(cv2.CAP_PROP_FRAME_WIDTH))
frame_height = int(video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
fps = video_capture.get(cv2.CAP_PROP_FPS)
fourcc = video_capture.get (cv2.CAP_PROP_FOURCC);
print ("frame_width=",frame_width,"frame_height=",frame_height,"fps=",fps,"fourcc=",hex(int(fourcc)))

if dest_fps < fps:
 print ("dest_fps < fps",dest_fps,"<",fps)
 video_capture.release()
 exit (1)

while True:
 ret, frame = video_capture.read()
 if not ret:
  break
 image_count += 1
 roi = frame
 b, g, r = cv2.split(roi)
 #hex_values_b = ['#{:02x}'.format(b_val) for b_val in b.flatten()]
 #hex_values_g = ['#{:02x}'.format(g_val) for g_val in g.flatten()]
 #hex_values_r = ['#{:02x}'.format(r_val) for r_val in r.flatten()]
 hex_values = ['#{:02x}{:02x}{:02x}'.format(b_val, g_val, r_val) for b_val, g_val, r_val in zip(b.flatten(), g.flatten(), r.flatten())]
 print ("frame=", image_count)
 frames.append (roi)
# have 27 frames, append last 3
for i in range (0,dest_fps-len(frames)):
 print ("append last frame=", i)
 frames.append (frames[int(fps)])

print("Saving GIF file")
with imageio.get_writer("smiling.gif", mode="I", fps=dest_fps) as writer:
 for idx, frame in enumerate(frames):
  print("Adding frame to GIF file:", idx + 1)
  rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
  writer.append_data(rgb_frame)

video_capture.release()
print ("Done")

"""
#https://medium.com/@sahilutekar.su/extracting-and-overlaying-hex-values-from-video-frames-c49fe33bfecc

import cv2
import numpy as np

class VideoProcessor:
    def __init__(self, video_path, output_video_path):
        self.video_path = video_path
        self.output_video_path = output_video_path
        self.roi_top_left = None
        self.roi_bottom_right = None
        self.fps = None
        self.frame_width = None
        self.frame_height = None
        self.video_capture = None
        self.output_video = None

    def select_roi(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            self.roi_top_left = (x, y)
        elif event == cv2.EVENT_LBUTTONUP:
            self.roi_bottom_right = (x, y)
            cv2.destroyAllWindows()

    def process_video(self):
        self.video_capture = cv2.VideoCapture(self.video_path)
        self.frame_width = int(self.video_capture.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.frame_height = int(self.video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.fps = self.video_capture.get(cv2.CAP_PROP_FPS)

        fourcc = cv2.VideoWriter_fourcc(*"mp4v")
        self.output_video = cv2.VideoWriter(self.output_video_path, fourcc, self.fps, (self.frame_width, self.frame_height))

        cv2.namedWindow("Select ROI")
        cv2.setMouseCallback("Select ROI", self.select_roi)

        while True:
            ret, frame = self.video_capture.read()

            if not ret:
                break

            cv2.imshow("Select ROI", frame)

            if cv2.waitKey(int(1000 / self.fps)) & 0xFF == ord('q'):
                break

            if self.roi_top_left is not None and self.roi_bottom_right is not None:
                roi = frame[self.roi_top_left[1]:self.roi_bottom_right[1], self.roi_top_left[0]:self.roi_bottom_right[0]]
                b, g, r = cv2.split(roi)

                hex_values = ['#{:02x}{:02x}{:02x}'.format(b_val, g_val, r_val) for b_val, g_val, r_val in zip(b.flatten(), g.flatten(), r.flatten())]

                for i, hex_val in enumerate(hex_values):
                    x = i % roi.shape[1]
                    y = i // roi.shape[1]
                    cv2.putText(roi, hex_val, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)

                text_bg = 255 * np.ones(roi.shape, dtype=np.uint8)
                text_with_bg = cv2.putText(text_bg, '\n'.join(hex_values), (0, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv2.LINE_AA)
                frame[self.roi_top_left[1]:self.roi_bottom_right[1], self.roi_top_left[0]:self.roi_bottom_right[0]] = text_with_bg

            cv2.imshow("Video with Hex Values", frame)
            self.output_video.write(frame)

        self.video_capture.release()
        self.output_video.release()
        cv2.destroyAllWindows()

# Example usage
video_path = "hw-vga.gif"
output_video_path = "smiling.gif"

processor = VideoProcessor(video_path, output_video_path)
processor.process_video()
"""

"""
#https://pysource.com/2021/03/25/create-an-animated-gif-in-real-time-with-opencv-and-python/

import numpy
import cv2 as cv
import imageio
import imageio.v3 as iio

cap = cv.VideoCapture('hw-vga.gif')
frames = []
image_count = 0
while cap.isOpened():
 ret, frame = cap.read()
 cv.imshow("frame", frame)
 key = cv.waitKey(0)
 if key == ord("a"):
  image_count += 1
  frames.append(frame)
  print("Adding new image:", image_count)
 elif key == ord("q"):
  break
print("Images added: ", len(frames))

print("Saving GIF file")
with imageio.get_writer("smiling.gif", mode="I") as writer:
 for idx, frame in enumerate(frames):
  print("Adding frame to GIF file: ", idx + 1)
  rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
  writer.append_data(rgb_frame)
"""

"""
import numpy
import cv2 as cv
import imageio
#
cap = cv.VideoCapture('hw-vga.gif')
# Define the codec and create VideoWriter object
#fourcc = cv.VideoWriter_fourcc('G','I','F')
#out = cv.VideoWriter('output.gif', fourcc, 10.0, (1920, 1080))
#update_img=[]
writer=imageio.get_writer("smiling.gif", mode="I")
while cap.isOpened():
    ret, frame = cap.read()
    # if frame is read correctly ret is True
    '''if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break'''
    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    ret,thresh1 = cv.threshold(gray,242,255,cv.THRESH_BINARY)
    #gray = cv.cvtColor(frame)
    tt=numpy.argwhere(thresh1 == 0)
    tt=tt[:,1]
    min_pos=min(tt)
    max_pos=max(tt)
    update_img=frame[0:1920,(min_pos-5):(max_pos+5)]
    writer.append_data(update_img)
    #out.write(update_img)
    #imageio.imwrite('gif.gif', update_img) 
    cv.imshow('frame', update_img)
    if cv.waitKey(1) == ord('q'):
        break
#imageio.imwrite('astronaut-gray.gif', update_img)
'''with imageio.get_writer("smiling.gif", mode="I") as writer:
    for idx, frame in enumerate(frames):
        print("Adding frame to GIF file: ", idx + 1)
        writer.append_data(frame)'''    
cap.release()
cv.destroyAllWindows()
"""

