#!/usr/bin/env python3

import numpy as np
import struct
import cv2

# https://g.co/gemini/share/a8ab41aecf37
# https://g.co/gemini/share/e34474e39e81
# https://g.co/gemini/share/e99148b91bdc

# https://www.slingacademy.com/article/using-numpy-frombuffer-function-5-examples/#Example_3_Interpreting_Floating_Point_Numbers
# https://docs.python.org/3/library/struct.html#
# https://stackoverflow.com/a/49653114
# https://www.delftstack.com/howto/python/opencv-create-image/
# https://stackoverflow.com/a/61521134
# https://stackoverflow.com/a/61816640
# https://pythonhint.com/post/4838135136474420/python-want-to-display-red-channel-only-in-opencv
# https://stackoverflow.com/questions/52632718/display-image-only-in-rred-channel-using-python
# https://stackoverflow.com/questions/26975769/modify-a-particular-row-column-of-a-numpy-array
# https://docs.opencv.org/3.4/d3/dc1/tutorial_basic_linear_transform.html

def hex_to_bytes(hex_string):
  """Converts a hexadecimal string (without the '#') to a list of 8-bit byte values.
  Args:
      hex_string (str): The hexadecimal string to convert (without the '#' prefix).
  Returns:
      list: A list of integers representing the 8-bit byte values.
  Raises:
      ValueError: If the input string is not a valid hexadecimal string or has a length that is not a multiple of 2.
  """
  if not all(c in '0123456789abcdefABCDEF' for c in hex_string):
    raise ValueError("Invalid hexadecimal string: {}".format(hex_string))
  if len(hex_string) % 2 != 0:
    raise ValueError("Hexadecimal string length must be a multiple of 2")
  # Split the string into groups of 2 characters (each representing a byte)
  byte_strings = [hex_string[i:i+2] for i in range(0, len(hex_string), 2)]
  # Convert each byte string to an integer (8-bit value)
  bytes = [int(byte_string, 16) for byte_string in byte_strings]
  return bytes
# Example usage (without the '#' prefix)
#hex_string = "94979b"
#bytes_list = hex_to_bytes(hex_string)
#print(bytes_list)  # Output: [170, 171, 204]

def int_to_hex(value):
  """Converts an integer to its hexadecimal representation.
  Args:
      value (int): The integer to convert.
  Returns:
      str: The hexadecimal representation of the integer (uppercase).
  """
  if not isinstance(value, int):
    raise TypeError("Input must be an integer")
  # Convert the integer to hexadecimal using format specifier 'X' for uppercase
  hex_string = format(value, 'X')
  return hex_string
# Example usage
#number = 255
#hex_value = int_to_hex(number)
#print(f"Hexadecimal representation of {number}: {hex_value}")  # Output: Hexadecimal representation of 255: FF

# RGB888 version
"""
list_pixel = []
with open("hex_memory_file_frame1.hex","r") as f:
	lines = f.readlines()
	for line in lines:
		#print (f"{line}")
		bytes_list = hex_to_bytes(line[1:7])
		#print (format(bytes_list[0], 'X'),format(bytes_list[1], 'X'),format(bytes_list[2], 'X'))
		list_pixel.append((bytes_list[0],bytes_list[1],bytes_list[2]))
	f.close()
	stream_buffer = bytearray()
	for i in list_pixel:
		pi = struct.pack('B',i[0])+struct.pack('B',i[1])+struct.pack('B',i[2])
		stream_buffer += pi
	arr = np.frombuffer(stream_buffer, dtype=np.uint8)
	arr.shape = (480,640,3)
	print (arr)
	cv2.imshow("Binary", arr)
	cv2.waitKey(0)
"""

def rgb565_to_int(red, green, blue):
  """Converts RGB565 color values (5 bits each for red, green, and blue) to a single integer.
  Args:
      red (int): Red value (0-31).
      green (int): Green value (0-63).
      blue (int): Blue value (0-31).
  Returns:
      int: The RGB565 integer representation.
  Raises:
      ValueError: If any of the RGB values are outside the valid range (0-31).
  """
  if not (0 <= red <= 31 and 0 <= green <= 63 and 0 <= blue <= 31):
    raise ValueError("RGB values must be between 0 and 31 (inclusive)")
  # Shift and combine the RGB values into a single integer
  rgb_int = (red << 11) | (green << 5) | blue
  return rgb_int
# Example usage (assuming valid RGB values)
red = 15  # Red value (5 bits)
green = 31  # Green value (6 bits)
blue = 23  # Blue value (5 bits)
rgb_integer = rgb565_to_int(red, green, blue)
print(f"RGB565 integer representation: {rgb_integer:0=16x}")  # Output: ffff8b (formatted hex)

# RGB565 version
list_pixel = []
with open("hex_memory_file_frame1.hex","r") as f:
	lines = f.readlines()
	for line in lines:
		#print (f"{line}")
		bytes_list = hex_to_bytes(line[1:7])
		#print (format(bytes_list[0], 'X'),format(bytes_list[1], 'X'),format(bytes_list[2], 'X'))
		list_pixel.append((bytes_list[0],bytes_list[1],bytes_list[2]))
	f.close()
	stream_buffer = bytearray()
	for i in list_pixel:
		pi = struct.pack('B',i[0])+struct.pack('B',i[1])+struct.pack('B',i[2])
		stream_buffer += pi
	arr = np.frombuffer(stream_buffer, dtype=np.uint8)
	#print (arr)
	arr.shape = (480,640,3)
	#print (arr)
	#cv2.imshow("Binary", arr) # color
	tmp_imr = np.zeros(arr.shape, dtype=np.uint8)
	tmp_img = np.zeros(arr.shape, dtype=np.uint8)
	tmp_imb = np.zeros(arr.shape, dtype=np.uint8)
	tmp_im = np.zeros(arr.shape, dtype=np.uint8)
	tmp_imr[:,:,2] = arr[:,:,2]
	tmp_img[:,:,1] = arr[:,:,1]
	tmp_imb[:,:,0] = arr[:,:,0]
	#cv2.imshow("Binary", tmp_imr) # extract RGB channel
	tmp_imr[:,:,2] = (arr[:,:,2]>>3)
	tmp_img[:,:,1] = (arr[:,:,1]>>2)
	tmp_imb[:,:,0] = (arr[:,:,0]>>3)
	tmp_im[:,:,2] = tmp_imr[:,:,2];
	tmp_im[:,:,1] = tmp_img[:,:,1];
	tmp_im[:,:,0] = tmp_imb[:,:,0];
	#print (tmp_imr)
	#print (tmp_img)
	#print (tmp_imb)
	#print (tmp_im)
	#cv2.imshow("Binary", tmp_im) # RGB565
	#for x in range (480):
	#	for y in range (640):
	#		rgb_int = tmp_imb[x,y,0] << 11 | tmp_img[x,y,1] << 5 | tmp_imr[x,y,2] # test values to compare
	#		print (f"{rgb_int:04x}")
	#exit (1)
	new_image = np.zeros(tmp_im.shape, tmp_im.dtype)
	alpha = 3.0 # Simple contrast control - alpha value [1.0-3.0]
	beta = 100 # Simple brightness control - beta value [0-100]
	for y in range(tmp_im.shape[0]):
		for x in range(tmp_im.shape[1]):
			for c in range(tmp_im.shape[2]):
				new_image[y,x,c] = np.clip(alpha*tmp_im[y,x,c] + beta, 0, 255)
	cv2.imshow("Binary", new_image) # RGB565 B/C
	cv2.waitKey(0)


    
    

