/*verilator tracing_off*/
// For test on verilator
`timescale 1ns/1ps
module camera_emu (
output o_vs,
output o_hs,
output o_pclk,
input i_xclk,
output [7:0] o_d,
input i_rst,
input i_pwdn
);

localparam
RAW_RGB=0,
tp = 2**RAW_RGB,
HREF1 = 640,
CHREF1 = HREF1 * tp,
HREF0 = 144,
CHREF0 = HREF0 * tp,
tline = CHREF1 + CHREF0,
CVSYNC1 = 3,
VSYNC1 = CVSYNC1 * tline,
CVSYNC2 = 17,
VSYNC2 = CVSYNC2 * tline,
CVSYNC3 = 480,
VSYNC3 = CVSYNC3 * tline,
CVSYNC4 = 10,
VSYNC4 = CVSYNC4 * tline,
CVSYNCALL = CVSYNC1 + CVSYNC2 + CVSYNC3 + CVSYNC4;

reg href_time;
reg pixel_time;

integer count1;
reg vvsync;
`define svs1 2'b00
`define svs2 2'b01
`define svs3 2'b10
`define svs4 2'b11
reg [1:0] state1;
//p.14 15 COM10 0x00 RW [2] - VSYNC changes on falling edge PCLK
wire VBIT;
assign VBIT = 1'b0; // toggle VSYNC camera

always @(posedge i_xclk or negedge i_rst) begin
		if (i_rst == 0) begin
			count1 = 0;
			vvsync <= VBIT;
			state1 = `svs1;
			href_time <= 0;
		end else begin
			case (state1)
				`svs1: begin
					vvsync <= ~VBIT;
					href_time <= 0;
					if (count1 == VSYNC1) begin
						state1 = `svs2;
						count1 = 0;
					end else begin
						state1 = `svs1;
						count1 = count1 + 1;
					end
				end
				`svs2: begin
					vvsync <= VBIT;
					href_time <= 0;
					if (count1 == VSYNC2) begin
						state1 = `svs3;
						count1 = 0;
					end else begin
						state1 = `svs2;
						count1 = count1 + 1;
					end
				end
				`svs3: begin
					vvsync <= VBIT;
					href_time <= 1;
					if (count1 == VSYNC3) begin
						state1 = `svs4;
						count1 = 0;
					end else begin
						state1 = `svs3;
						count1 = count1 + 1;
					end
				end
				`svs4: begin
					vvsync <= VBIT;
					href_time <= 1'b0;
					if (count1 == VSYNC4) begin
						state1 = `svs1;
						count1 = 0;
					end else begin
						state1 = `svs4;
						count1 = count1 + 1;
					end
				end
				default:;
			endcase
		end
	end
	assign o_vs = vvsync;

	`define swait4vsync 2'b00
	`define shref1 2'b01
	`define shref0 2'b10
	reg [1:0] state2;
	integer count2,counth1,counth0;
	reg vhref;
		
	// generate href pulse
	// on falling edge
	always @(negedge i_rst or negedge i_xclk) begin
		if (i_rst == 0) begin
			count2 = 0;
			counth1 = 0;
			counth0 = 0;
			state2 = `swait4vsync;
			vhref <= 0;
		end else begin
			case (state2)
				`swait4vsync: begin
					pixel_time <= 0;
					if (href_time == 1) begin
						if (count2 == VSYNC3) begin
							state2 = `swait4vsync;
							count2 = 0;
						end else begin
							state2 = `shref1;
							pixel_time <= 1;
							count2 = count2 + 1;
						end
					end else begin
						state2 = `swait4vsync;
					end
				end
				`shref1: begin
					pixel_time <= 1;
					vhref <= 1;
					if (counth1 == CHREF1 - 1) begin
						pixel_time <= 0;
						state2 = `shref0;
						counth1 = 0;
					end	else begin
						state2 = `shref1;
						counth1 = counth1 + 1;
					end
				end
				`shref0: begin
					pixel_time <= 0;
					vhref <= 0;
					if (counth0 == CHREF0 - 1) begin
						state2 = `swait4vsync;
						counth0 = 0;
					end else begin
						state2 = `shref0;
						counth0 = counth0 + 1;
					end
				end
				default:;
			endcase
		end
	end
	assign o_hs = vhref;

	localparam
	CDATALENGTH = 5,
	CNUMPIXELS = HREF1 - CDATALENGTH * 2;
	integer count3;
	wire [7:0] startdata [0:CDATALENGTH - 1];
	assign startdata[0] = 8'hFF;
	assign startdata[1] = 8'hEE;
	assign startdata[2] = 8'hDD;
	assign startdata[3] = 8'hCC;
	assign startdata[4] = 8'hBB;
	wire [7:0] enddata [0:CDATALENGTH - 1];
	assign enddata[0] = 8'hBB;
	assign enddata[1] = 8'hCC;
	assign enddata[2] = 8'hDD;
	assign enddata[3] = 8'hEE;
	assign enddata[4] = 8'hFF;
	wire [7:0] odddata  = 8'hAA;
	wire [7:0] evendata = 8'h55;
	`define s1 2'b00
	`define s2 2'b01
	`define s3 2'b10
	reg [1:0] state3;
	reg [7:0] vd;

	always @(negedge i_rst or negedge i_xclk) begin
		if (i_rst == 0) begin
			vd = 8'h00;
			state3 = `s1;
			count3 = 0;
		end else begin
			if (pixel_time == 1) begin
				case (state3)
					`s1: begin
						vd = startdata[count3];
						if (count3 == CDATALENGTH - 1) begin
							count3 = 0;
							state3 = `s2;
						end else begin
							count3 = count3 + 1;
							state3 = `s1;
						end
					end
					`s2: begin
						if (count3 == CNUMPIXELS - 1) begin
							state3 = `s3;
							count3 = 0;
						end else begin
							state3 = `s2;
							if (count3 % 2 == 0) begin
								vd = odddata;
								count3 = count3 + 1;
							end else if (count3 % 2 == 1) begin
								vd = evendata;
								count3 = count3 + 1;
							end else begin
								vd = 8'h00;
							end
						end
					end
					`s3: begin
						vd = enddata[count3];
						if (count3 == CDATALENGTH - 1) begin
							count3 = 0;
							state3 = `s1;
						end else begin
							count3 = count3 + 1;
							state3 = `s3;
						end
					end
				default:;
				endcase
			end else begin
				vd = 8'h00;
			end
		end
	end
	assign o_d = vd;

  assign o_pclk = i_xclk;

endmodule

