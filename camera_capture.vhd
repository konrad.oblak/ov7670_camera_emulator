-------------------------------------------------------------------------------
-- Company:       -
-- Engineer:      ko
--
-- Create Date:   13:25:06 05/09/2024
-- Design Name:   Very simple OV7670 camera emulator (IP Core) Testbench
-- Module Name:   camera_capture - Behavioral
-- Project Name:  ov7670_camera_emulator
-- Target Device: Synthesizable code
-- Tool versions: Xilinx ISE 14.7
-- Description:   Used to capture data from camera.
--                Base: github.com/jasonsetiawan/ov7670_vga_Nexys2/blob/master/
--                ov7670_capture.vhd
--
-- Denotes used in all code (only first/last prefix/suffix):
--  - c_   - constants
--  - g_   - generates
--  - i_   - input signals
--  - o_   - output signals
--  - p_   - processes
--  - v_   - variables
--  - io_  - inout signals
--  - lX_  - loops
--  - _i   - internal signals
--  - _iX  - instances
--  - _m   - memories
--  - _p   - packages
--  - _sr  - shift registers
--  - _t   - types
--  - _dut - Device Under Test
--  - _uut - Unit Under Test
--
-- Dependencies:
--  - Files:
--    - numeric_std.vhd - version without debug enabled.
--    - p_constants.vhd - constants for camera device.
--  - Modules: -
--
-- Revision:
--  - Revision 0.01 - File created
--    - Files: -
--    - Modules: -
--    - Processes (Architecture: behavioral):
--      - p_camera_capture - main process.
--
-- Imporant Subtypes/Types/Signals/Variables/Constants: -
--
-- Information from the software vendor:
--  - Messeges: -
--  - Bugs: -
--  - Notices: -
--  - Infos: -
--  - Notes: -
--  - Criticals/Failures: -
--
-- Concepts/Milestones: -
--
-- Additional Comments:
--  - Data from camera is latched on falling edge PCLK and return 16bit on
--    rising edge PCLK.
--
-------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;
  use work.numeric_std.all;
  use work.p_constants.all;

entity camera_capture is
  port (
    i_camera_rst                  : in    std_logic;
    i_camera_pclk                 : in    std_logic;
    i_camera_vsync                : in    std_logic;
    i_camera_href                 : in    std_logic;
    i_camera_data                 : in    std_logic_vector(7 downto 0);
    o_camera_data                 : out   std_logic_vector(15 downto 0);
    o_camera_address_write_enable : out   std_logic;
    o_camera_address_counter      : out   std_logic_vector(c_camera_frame_memory_data_bits - 1 downto 0)
  );
end entity camera_capture;

architecture behavioral of camera_capture is

  signal camera_address_counter_i      : std_logic_vector(c_camera_frame_memory_data_bits - 1 downto 0);
  signal camera_address_write_enable_i : std_logic;
  signal camera_data_i                 : std_logic_vector(c_bits_color_rgb565 - 1 downto 0);
  signal camera_data_latch_i           : std_logic_vector(15 downto 0);
  signal camera_href_hold_i            : std_logic;
  signal camera_latched_data_i         : std_logic_vector(7 downto 0);
  signal camera_latched_vsync_i        : std_logic;
  signal camera_latched_href_i         : std_logic;

begin

  o_camera_address_counter <= camera_address_counter_i;
  o_camera_address_write_enable <= camera_address_write_enable_i;
  o_camera_data <= camera_data_latch_i;

  p_camera_capture : process (i_camera_rst, i_camera_pclk) is
  begin

    if (i_camera_rst = '0') then
      camera_address_counter_i      <= (others => '0');
      camera_address_write_enable_i <= '0';
      camera_href_hold_i            <= '0';
      camera_data_latch_i           <= (others => '0');
      camera_latched_data_i         <= (others => '0');
      camera_latched_href_i         <= '0';
      camera_latched_vsync_i        <= '0';
    elsif (rising_edge (i_camera_pclk)) then
      if (camera_address_write_enable_i = '1') then
        camera_address_counter_i <= std_logic_vector(unsigned (camera_address_counter_i) + 1);
      end if;
      camera_href_hold_i <= camera_latched_href_i;
      if (camera_href_hold_i = '1') then
        camera_data_latch_i           <= camera_data_latch_i(7 downto 0) & camera_latched_data_i;
        camera_address_write_enable_i <= not camera_address_write_enable_i;
      else
        camera_data_latch_i           <= (others => '0');
        camera_address_write_enable_i <= '0';
      end if;
      if (camera_latched_vsync_i = '1') then
        camera_address_counter_i <= (others => '0');
      end if;
    end if;

    if (falling_edge (i_camera_pclk)) then
      camera_latched_data_i  <= i_camera_data;
      camera_latched_href_i  <= i_camera_href;
      camera_latched_vsync_i <= i_camera_vsync;
    end if;

  end process p_camera_capture;

end architecture behavioral;
