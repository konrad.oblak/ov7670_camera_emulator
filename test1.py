#!/usr/bin/env python3

import imageio.v3 as iio
from ppretty import ppretty

metadata = iio.immeta ('hw-vga.gif')
print (ppretty (metadata))
im = iio.imread ('hw-vga.gif')
print (ppretty (im.shape)) # (27, 480, 640, 3)
gif_encoded = iio.imwrite("<bytes>", im, extension=".gif")

#for frame in iio.imiter("hw-vga.gif"):
# hex_data = " ".join(f"{b:02x}" for b in frame)
# print (hex_data)

for frame in iio.imiter("hw-vga.gif"):
 print (ppretty (frame))
#    print (ppretty (frame,indent='    ', depth=2, width=30, seq_length=6, show_protected=True, show_private=False, show_static=True, show_properties=True, show_address=True))


"""
 
import imageio
import io

def get_gif_raw_binary(gif_file):
  Reads a GIF file and returns its raw binary data.

  Args:
      gif_file (str): Path to the GIF file.

  Returns:
      bytes: Raw binary data of the GIF file.

  Raises:
      FileNotFoundError: If the GIF file is not found.
      RuntimeError: If there's an error reading the GIF file.


  try:
    with open(gif_file, "rb") as f:
      # Read the entire GIF file into a byte buffer
      gif_data = io.BytesIO(f.read())
  except FileNotFoundError:
    raise FileNotFoundError(f"GIF file not found: {gif_file}")
  else:
    # Attempt to read the GIF using imageio
    try:
      reader = imageio.get_reader(gif_data)
    except RuntimeError:
      raise RuntimeError(f"Error reading GIF file: {gif_file}")
    else:
      # Close the reader as we don't need the image data itself
      #reader.close()
      raw_binary = reader.getvalue()  # Return the raw binary data
      
      # Convert each byte to its hex representation (two hex digits)
      hex_data = " ".join(f"{b:02x}" for b in raw_binary[0:20])
      return raw_binary, hex_data

# Example usage
gif_file = "hw-vga.gif"
raw_binary, hex_data = get_gif_raw_binary(gif_file)

print(f"Raw binary data type: {type(raw_binary)}")
print(f"Raw binary data (hex): {hex_data}")
#print(f"Raw binary data type: {type(raw_binary)}")
#print(f"Length of raw binary data: {len(raw_binary)} bytes")

# You can now use the raw_binary data as needed
# (e.g., write it to a new file, send it over a network, etc.)
"""

