-------------------------------------------------------------------------------
-- Company:        -
-- Engineer:       ko
--
-- Create Date:    19:22:33 05/03/2024
-- Design Name:    Very simple OV7670 camera emulator (IP Core) Testbench
-- Module Name:    memory_module - Behavioral (synthesizable)
-- Project Name:   ov7670_camera_emulator
-- Target Devices: Non-synthesizable
-- Tool versions:  Xilinx ISE 14.7
-- Description:    Dual-port Memory behavioral based on Xilinx documentaion.
--                 Dual-Port ram with Synchronous Read (Read Through)
--                 using More than One Clock (rama_12).
-- Dependencies:
--  - Files: -
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--  - Xilinx UG627 PDF page 153
--  - Code copied from Xilinx UG627 PDF page 153.
--  - Fix for simulation: use 'shared variable' for 'ram'.
--
-------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;

entity rama_12 is
  generic (
    constant addr_bits : integer := 0;
    constant data_bits : integer := 0
  );
  port (
    do1  : out   std_logic_vector(data_bits - 1 downto 0);
    do2  : out   std_logic_vector(data_bits - 1 downto 0);
    di   : in    std_logic_vector(data_bits - 1 downto 0);
    add1 : in    std_logic_vector(addr_bits - 1 downto 0);
    add2 : in    std_logic_vector(addr_bits - 1 downto 0);
    clk1 : in    std_logic;
    clk2 : in    std_logic;
    we   : in    std_logic
  );
end entity rama_12;

architecture synthesizable of rama_12 is

  type ram_type is
    array (0 to 2 ** addr_bits - 1) of
      std_logic_vector(data_bits - 1 downto 0);

  shared variable ram : ram_type;

  signal read_add1 : std_logic_vector(addr_bits - 1 downto 0);
  signal read_add2 : std_logic_vector(addr_bits - 1 downto 0);

begin

  p_rama_12_port_a : process (clk1) is
  begin

    if (rising_edge (clk1)) then
      if (we = '1') then
        ram (conv_integer (add1)) := di;
      end if;
      read_add1 <= add1;
    end if;

  end process p_rama_12_port_a;

  do1 <= ram (conv_integer (read_add1));

  p_rama_12_port_b : process (clk2) is
  begin

    if (rising_edge (clk2)) then
      read_add2 <= add2;
    end if;

  end process p_rama_12_port_b;

  do2 <= ram (conv_integer (read_add2));

end architecture synthesizable;
