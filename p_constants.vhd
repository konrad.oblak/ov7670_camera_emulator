-------------------------------------------------------------------------------
--
--  Package File: p_constants.vhd
--
--  Purpose: This file defines supplemental type,subtype,constant,function ('s)
--    for module : ov7670_camera_emulator.
--
--  Denotes:
--    - c_ - constants
--    - _t  - types
--    - _m - memories
--    - _i - internal signals
--    - v_ - variables
--    - i_ - input signals
--    - o_ - output signals
--    - _iX - instances
--    - g_ - generates
--    - _sr - shift registers
--    - _uut - Unit Under Test
--    - _dut - Device Under Test
--    - lX_ - loops
--
-- Additional Comments:
--  - Module is only for simulation.
--  - Module load *.hex data for simulation.
--    Each .hex file is RAW data storing one frame 640x480 with RGB888 color.
--    Is 30 .hex files, so we have 1 second animation (camera have max 30 fps)
--    On this time, only one frame is used:
--      - from file hex_memory_file_frame1.hex
--      - c_hex_rom_files_count = 1
--
-- Dependencies:
--  - Files:
--    - numeric_std.vhd
--      copy of with commented lines 3211-3213 and 3181-3182
--      for boost ISIM simulation, but better is set flag NO_WARNING (line 883)
--    - hex_memory_file_frame*.hex
--      files with RAW data for each frame - store RAW data for VGA 30 fps
--  - Modules:
--    - STD_LOGIC_TEXTIO - for 'hstring' function
--
-- Revision:
--  - Revision 0.01 - File created
--    - Files: -
--    - Modules: -
--    - Functions:
--      - hex - convert std_logic_vector to HEX string
--      - to_string_1 - convert std_logic_vector as raw bits string
--    - Procedures:
--      - readandconvertrom - create ROM memory from files
--
-- Concepts/Milestones:
--  - for captured_frame_mem object, probe to use CONSTANT
--
-- Imporant Subtypes/Types/Signals/Variables/Constants:
--  - c_debug - flag which turn on some debugging on console
--  - c_hex_rom_files_name - ROM file name (each line have '#AABBCC')
--  - c_hex_rom_files_ext - ROM file name suffix (file extension - using .hex)
--  - c_hex_rom_files_count - numer files
--    - For now, in TB we assume import 1 frame.
--  - camera_frame_t - 2d array store 640x480 RGB565 raw pixels
--  - captured_frame_mem - initialized empty memory for camera_frame_t
--
-- Information from the software vendor:
--  - Messeges: -
--  - Bugs: -
--  - Notices: -
--  - Infos: -
--  - Notes: -
--  - Criticals/Failures: -
--
-------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_textio.all;
  use work.numeric_std.all;
  use std.textio.all;

package p_constants is

  constant c_debug                           : boolean := false;
  constant c_bits_color_rgb888               : integer := 24;
  constant c_bits_color_rgb565               : integer := 16;
  constant c_camera_width                    : integer := 640;
  constant c_camera_height                   : integer := 480;
  constant c_camera_fps                      : integer := 30;
  constant c_camera_frame_memory_data_bits   : integer := 21;
  constant c_camera_color_bits               : integer := c_bits_color_rgb565;
  constant c_camera_frame_length             : integer := c_camera_width * c_camera_height;
  constant c_camera_frame_length_with_colors : integer := c_camera_width * c_camera_height * 3;
  constant c_hex_rom_files_name              : string  := "hex_memory_file_frame";
  constant c_hex_rom_files_ext               : string  := "hex";
  constant c_hex_rom_files_count             : integer := 1;
  constant c_hex_rom_files_data_width        : integer := c_bits_color_rgb888;

  type camera_frame_t is
    array (0 to c_camera_width * c_camera_height - 1) of
      std_logic_vector(c_camera_color_bits - 1 downto 0);

  shared variable captured_frame_m : camera_frame_t;

  function to_string_1 (
    s : std_logic_vector
  ) return string;

  function hex (
    lvec : in std_logic_vector
  ) return string;

  procedure readandconvertrom (
    filename   : in string;
    start_addr : in integer
  );

end package p_constants;

package body p_constants is

  function to_string_1 (
    s : std_logic_vector
  ) return string is

    variable r : string (s'length downto 1);

  begin

    for i in s'range loop

      r (i + 1) := std_logic'image (s(i)) (2);

    end loop;

    return r;

  end function to_string_1;

  -- https://stackoverflow.com/a/53391980

  function hex (
    lvec : in std_logic_vector
  ) return string is

    subtype  halfbyte is std_logic_vector(4 - 1 downto 0);
    variable text : string (lvec'length / 4 - 1 downto 0);

  begin

    assert lvec'length mod 4 = 0
      report "hex() works only with vectors whose length is a multiple of 4"
      severity FAILURE;
    text := (others => '9');

    for k in text'range loop

      case halfbyte'(lvec(4 * k + 3 downto 4 * k)) is

        when "0000" =>

          text(k) := '0';

        when "0001" =>

          text(k) := '1';

        when "0010" =>

          text(k) := '2';

        when "0011" =>

          text(k) := '3';

        when "0100" =>

          text(k) := '4';

        when "0101" =>

          text(k) := '5';

        when "0110" =>

          text(k) := '6';

        when "0111" =>

          text(k) := '7';

        when "1000" =>

          text(k) := '8';

        when "1001" =>

          text(k) := '9';

        when "1010" =>

          text(k) := 'A';

        when "1011" =>

          text(k) := 'B';

        when "1100" =>

          text(k) := 'C';

        when "1101" =>

          text(k) := 'D';

        when "1110" =>

          text(k) := 'E';

        when "1111" =>

          text(k) := 'F';

        when others =>

          text(k) := '!';

      end case;

    end loop;

    return text;

  end function hex;

  procedure readandconvertrom (

    filename   : in string;
    start_addr : in integer

  ) is

    -- https://stackoverflow.com/a/32249431
    -- https://stackoverflow.com/a/41847365
    variable rgb888 : std_logic_vector(c_bits_color_rgb888 - 1 downto 0);
    variable rgb565 : std_logic_vector(c_bits_color_rgb565 - 1 downto 0);
    variable rgb565_r : std_logic_vector(4 downto 0);
    variable rgb565_g : std_logic_vector(5 downto 0);
    variable rgb565_b : std_logic_vector(4 downto 0);
    file F : text;
    variable fstatus : file_open_status;
    variable file_line : line;
    variable file_line1 : line;
    variable line_string : string (1 to 7);
    variable line_string1 : string (1 to 7);
    variable v_start_address : integer;

  begin

    v_start_address := start_addr;
    file_open (fstatus, F, filename);

    while not endfile (F) loop

      line_string1 := (others => ' ');
      readline (F, file_line);
      read (file_line, line_string);

      if (line_string'length > 0) then
        if (line_string (1) = character ('#')) then
          file_line1         := new string (1 to 7);
          line_string1       := line_string (2) &
                                line_string (3) &
                                line_string (4) &
                                line_string (5) &
                                line_string (6) &
                                line_string (7);
          file_line1.all (1) := line_string1 (1);
          file_line1.all (2) := line_string1 (2);
          file_line1.all (3) := line_string1 (3);
          file_line1.all (4) := line_string1 (4);
          file_line1.all (5) := line_string1 (5);
          file_line1.all (6) := line_string1 (6);
          file_line1.all (7) := line_string1 (7);
        end if;
      end if;

      hread (file_line1, rgb888);
      rgb565_r                             := rgb888 (23 downto 19);
      rgb565_g                             := rgb888 (15 downto 10);
      rgb565_b                             := rgb888 (7 downto 3);
      rgb565                               := rgb565_r & rgb565_g & rgb565_b;
      captured_frame_m (v_start_address)   := rgb565;
      v_start_address                      := v_start_address + 1;

      if (c_debug = true) then
        report "rgb888 " & " " & hex (rgb888) & " " & to_string_1 (rgb888);
        report "rgb565 " & " " & hex (rgb565) & " " & to_string_1 (rgb565);
      end if;

    end loop;

    file_close (F);
    report "Done";

  end procedure readandconvertrom;

end package body p_constants;
