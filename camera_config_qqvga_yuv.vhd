----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:34:43 07/20/2022 
-- Design Name: 
-- Module Name:    config - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.camera_regs.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity camera_config_qqvga_yuv is
port (
	i_clock : in std_logic;
	i_reset : in std_logic;
	i_address : in std_logic_vector(9 downto 0);
	o_data : out std_logic_vector(8 downto 0)
);
end camera_config_qqvga_yuv;

architecture Behavioral of camera_config_qqvga_yuv is
begin
i2c_data_process : process (i_clock,i_reset)
begin
if (i_reset = '1') then
o_data <= (others => '0');
elsif rising_edge(i_clock) then
case i_address is

when "00"&std_logic_vector(to_unsigned(  0,8)) => o_data <= "011100100"; -- delay
when "00"&std_logic_vector(to_unsigned(  1,8)) => o_data <= "101000010"; -- 0x42
when "00"&std_logic_vector(to_unsigned(  2,8)) => o_data <= "100010010"; -- 0x12 COM7
when "00"&std_logic_vector(to_unsigned(  3,8)) => o_data <= "110000000"; -- reset all regs
when "00"&std_logic_vector(to_unsigned(  4,8)) => o_data <= "011111111"; -- send
when "00"&std_logic_vector(to_unsigned(  5,8)) => o_data <= "011101001"; -- delay long

when "00"&std_logic_vector(to_unsigned(  6,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(  7,8)) => o_data <= "1"&REG_COM7;
when "00"&std_logic_vector(to_unsigned(  8,8)) => o_data <= "1"&COM7_YUV;
when "00"&std_logic_vector(to_unsigned(  9,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 10,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 11,8)) => o_data <= "1"&REG_RGB444;
when "00"&std_logic_vector(to_unsigned( 12,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned( 13,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 14,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 15,8)) => o_data <= "1"&REG_COM15;
when "00"&std_logic_vector(to_unsigned( 16,8)) => o_data <= "1"&COM15_R00FF;
when "00"&std_logic_vector(to_unsigned( 17,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 18,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 19,8)) => o_data <= "1"&REG_TSLB;
when "00"&std_logic_vector(to_unsigned( 20,8)) => o_data <= "1"&TSLB_YLAST;
when "00"&std_logic_vector(to_unsigned( 21,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 22,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 23,8)) => o_data <= "1"&REG_COM1;
when "00"&std_logic_vector(to_unsigned( 24,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned( 25,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 26,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 27,8)) => o_data <= "1"&REG_COM9;
when "00"&std_logic_vector(to_unsigned( 28,8)) => o_data <= "1"&x"68";
when "00"&std_logic_vector(to_unsigned( 29,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 30,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 31,8)) => o_data <= "1"&REG_BRIGHT;
when "00"&std_logic_vector(to_unsigned( 32,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned( 33,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 34,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 35,8)) => o_data <= "1"&REG_CMATRIX_1;
when "00"&std_logic_vector(to_unsigned( 36,8)) => o_data <= "1"&x"80";
when "00"&std_logic_vector(to_unsigned( 37,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 38,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 39,8)) => o_data <= "1"&REG_CMATRIX_2;
when "00"&std_logic_vector(to_unsigned( 40,8)) => o_data <= "1"&x"80";
when "00"&std_logic_vector(to_unsigned( 41,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 42,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 43,8)) => o_data <= "1"&REG_CMATRIX_3;
when "00"&std_logic_vector(to_unsigned( 44,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned( 45,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 46,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 47,8)) => o_data <= "1"&REG_CMATRIX_4;
when "00"&std_logic_vector(to_unsigned( 48,8)) => o_data <= "1"&x"22";
when "00"&std_logic_vector(to_unsigned( 49,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 50,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 51,8)) => o_data <= "1"&REG_CMATRIX_5;
when "00"&std_logic_vector(to_unsigned( 52,8)) => o_data <= "1"&x"5e";
when "00"&std_logic_vector(to_unsigned( 53,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 54,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 55,8)) => o_data <= "1"&REG_CMATRIX_6;
when "00"&std_logic_vector(to_unsigned( 56,8)) => o_data <= "1"&x"80";
when "00"&std_logic_vector(to_unsigned( 57,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 58,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 59,8)) => o_data <= "1"&REG_COM13;
when "00"&std_logic_vector(to_unsigned( 60,8)) => o_data <= "1"&(COM13_GAMMA or COM13_UVSAT or COM13_UVSWAP);
when "00"&std_logic_vector(to_unsigned( 61,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 62,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 63,8)) => o_data <= "1"&REG_HSTART;
when "00"&std_logic_vector(to_unsigned( 64,8)) => o_data <= "1"&x"16";
when "00"&std_logic_vector(to_unsigned( 65,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 66,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 67,8)) => o_data <= "1"&REG_HSTOP;
when "00"&std_logic_vector(to_unsigned( 68,8)) => o_data <= "1"&x"04";
when "00"&std_logic_vector(to_unsigned( 69,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 70,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 71,8)) => o_data <= "1"&REG_HREF;
when "00"&std_logic_vector(to_unsigned( 72,8)) => o_data <= "1"&x"24";
when "00"&std_logic_vector(to_unsigned( 73,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 74,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 75,8)) => o_data <= "1"&REG_VSTART;
when "00"&std_logic_vector(to_unsigned( 76,8)) => o_data <= "1"&x"02";
when "00"&std_logic_vector(to_unsigned( 77,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 78,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 79,8)) => o_data <= "1"&REG_VSTOP;
when "00"&std_logic_vector(to_unsigned( 80,8)) => o_data <= "1"&x"7a";
when "00"&std_logic_vector(to_unsigned( 81,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 82,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 83,8)) => o_data <= "1"&REG_VREF;
when "00"&std_logic_vector(to_unsigned( 84,8)) => o_data <= "1"&x"0a";
when "00"&std_logic_vector(to_unsigned( 85,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 86,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 87,8)) => o_data <= "1"&REG_COM3;
when "00"&std_logic_vector(to_unsigned( 88,8)) => o_data <= "1"&COM3_QQVGA;
when "00"&std_logic_vector(to_unsigned( 89,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 90,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 91,8)) => o_data <= "1"&REG_COM14;
when "00"&std_logic_vector(to_unsigned( 92,8)) => o_data <= "1"&COM14_QQVGA;
when "00"&std_logic_vector(to_unsigned( 93,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 94,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 95,8)) => o_data <= "1"&REG_SCALING_XSC;
when "00"&std_logic_vector(to_unsigned( 96,8)) => o_data <= "1"&SCALING_XSC_QQVGA;
when "00"&std_logic_vector(to_unsigned( 97,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned( 98,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned( 99,8)) => o_data <= "1"&REG_SCALING_YSC;
when "00"&std_logic_vector(to_unsigned(100,8)) => o_data <= "1"&SCALING_YSC_QQVGA;
when "00"&std_logic_vector(to_unsigned(101,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(102,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(103,8)) => o_data <= "1"&REG_SCALING_DCWCTR;
when "00"&std_logic_vector(to_unsigned(104,8)) => o_data <= "1"&SCALING_DCWCTR_QQVGA;
when "00"&std_logic_vector(to_unsigned(105,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(106,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(107,8)) => o_data <= "1"&REG_SCALING_PCLK_DIV;
when "00"&std_logic_vector(to_unsigned(108,8)) => o_data <= "1"&SCALING_PCLK_DIV_QQVGA;
when "00"&std_logic_vector(to_unsigned(109,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(110,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(111,8)) => o_data <= "1"&REG_SCALING_PCLK_DELAY;
when "00"&std_logic_vector(to_unsigned(112,8)) => o_data <= "1"&SCALING_PCLK_DELAY_QQVGA;
when "00"&std_logic_vector(to_unsigned(113,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(114,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(115,8)) => o_data <= "1"&REG_COM10;
when "00"&std_logic_vector(to_unsigned(116,8)) => o_data <= "1"&COM10_VS_NEG;
when "00"&std_logic_vector(to_unsigned(117,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(118,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(119,8)) => o_data <= "1"&x"7a";
when "00"&std_logic_vector(to_unsigned(120,8)) => o_data <= "1"&x"20";
when "00"&std_logic_vector(to_unsigned(121,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(122,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(123,8)) => o_data <= "1"&x"7b";
when "00"&std_logic_vector(to_unsigned(124,8)) => o_data <= "1"&x"10";
when "00"&std_logic_vector(to_unsigned(125,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(126,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(127,8)) => o_data <= "1"&x"7c";
when "00"&std_logic_vector(to_unsigned(128,8)) => o_data <= "1"&x"1e";
when "00"&std_logic_vector(to_unsigned(129,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(130,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(131,8)) => o_data <= "1"&x"7d";
when "00"&std_logic_vector(to_unsigned(132,8)) => o_data <= "1"&x"35";
when "00"&std_logic_vector(to_unsigned(133,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(134,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(135,8)) => o_data <= "1"&x"7e";
when "00"&std_logic_vector(to_unsigned(136,8)) => o_data <= "1"&x"5a";
when "00"&std_logic_vector(to_unsigned(137,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(138,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(139,8)) => o_data <= "1"&x"7f";
when "00"&std_logic_vector(to_unsigned(140,8)) => o_data <= "1"&x"69";
when "00"&std_logic_vector(to_unsigned(141,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(142,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(143,8)) => o_data <= "1"&x"80";
when "00"&std_logic_vector(to_unsigned(144,8)) => o_data <= "1"&x"76";
when "00"&std_logic_vector(to_unsigned(145,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(146,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(147,8)) => o_data <= "1"&x"81";
when "00"&std_logic_vector(to_unsigned(148,8)) => o_data <= "1"&x"80";
when "00"&std_logic_vector(to_unsigned(149,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(150,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(151,8)) => o_data <= "1"&x"82";
when "00"&std_logic_vector(to_unsigned(152,8)) => o_data <= "1"&x"88";
when "00"&std_logic_vector(to_unsigned(153,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(154,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(155,8)) => o_data <= "1"&x"83";
when "00"&std_logic_vector(to_unsigned(156,8)) => o_data <= "1"&x"8f";
when "00"&std_logic_vector(to_unsigned(157,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(158,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(159,8)) => o_data <= "1"&x"84";
when "00"&std_logic_vector(to_unsigned(160,8)) => o_data <= "1"&x"96";
when "00"&std_logic_vector(to_unsigned(161,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(162,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(163,8)) => o_data <= "1"&x"85";
when "00"&std_logic_vector(to_unsigned(164,8)) => o_data <= "1"&x"a3";
when "00"&std_logic_vector(to_unsigned(165,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(166,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(167,8)) => o_data <= "1"&x"86";
when "00"&std_logic_vector(to_unsigned(168,8)) => o_data <= "1"&x"af";
when "00"&std_logic_vector(to_unsigned(169,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(170,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(171,8)) => o_data <= "1"&x"87";
when "00"&std_logic_vector(to_unsigned(172,8)) => o_data <= "1"&x"c4";
when "00"&std_logic_vector(to_unsigned(173,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(174,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(175,8)) => o_data <= "1"&x"88";
when "00"&std_logic_vector(to_unsigned(176,8)) => o_data <= "1"&x"d7";
when "00"&std_logic_vector(to_unsigned(177,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(178,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(179,8)) => o_data <= "1"&x"89";
when "00"&std_logic_vector(to_unsigned(180,8)) => o_data <= "1"&x"e8";
when "00"&std_logic_vector(to_unsigned(181,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(182,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(183,8)) => o_data <= "1"&REG_COM8;
when "00"&std_logic_vector(to_unsigned(184,8)) => o_data <= "1"&(COM8_FASTAEC or COM8_AECSTEP or COM8_BFILT);
when "00"&std_logic_vector(to_unsigned(185,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(186,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(187,8)) => o_data <= "1"&REG_GAIN;
when "00"&std_logic_vector(to_unsigned(188,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(189,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(190,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(191,8)) => o_data <= "1"&REG_AECH;
when "00"&std_logic_vector(to_unsigned(192,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(193,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(194,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(195,8)) => o_data <= "1"&REG_COM4;
when "00"&std_logic_vector(to_unsigned(196,8)) => o_data <= "1"&x"40";
when "00"&std_logic_vector(to_unsigned(197,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(198,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(199,8)) => o_data <= "1"&REG_BD50MAX;
when "00"&std_logic_vector(to_unsigned(200,8)) => o_data <= "1"&x"05";
when "00"&std_logic_vector(to_unsigned(201,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(202,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(203,8)) => o_data <= "1"&REG_BD60MAX;
when "00"&std_logic_vector(to_unsigned(204,8)) => o_data <= "1"&x"07";
when "00"&std_logic_vector(to_unsigned(205,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(206,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(207,8)) => o_data <= "1"&REG_AEW;
when "00"&std_logic_vector(to_unsigned(208,8)) => o_data <= "1"&x"95";
when "00"&std_logic_vector(to_unsigned(209,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(210,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(211,8)) => o_data <= "1"&REG_AEB;
when "00"&std_logic_vector(to_unsigned(212,8)) => o_data <= "1"&x"33";
when "00"&std_logic_vector(to_unsigned(213,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(214,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(215,8)) => o_data <= "1"&REG_VPT;
when "00"&std_logic_vector(to_unsigned(216,8)) => o_data <= "1"&x"e3";
when "00"&std_logic_vector(to_unsigned(217,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(218,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(219,8)) => o_data <= "1"&REG_HAECC1;
when "00"&std_logic_vector(to_unsigned(220,8)) => o_data <= "1"&x"78";
when "00"&std_logic_vector(to_unsigned(221,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(222,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(223,8)) => o_data <= "1"&REG_HAECC2;
when "00"&std_logic_vector(to_unsigned(224,8)) => o_data <= "1"&x"68";
when "00"&std_logic_vector(to_unsigned(225,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(226,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(227,8)) => o_data <= "1"&x"a1";
when "00"&std_logic_vector(to_unsigned(228,8)) => o_data <= "1"&x"03";
when "00"&std_logic_vector(to_unsigned(229,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(230,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(231,8)) => o_data <= "1"&REG_HAECC3;
when "00"&std_logic_vector(to_unsigned(232,8)) => o_data <= "1"&x"d8";
when "00"&std_logic_vector(to_unsigned(233,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(234,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(235,8)) => o_data <= "1"&REG_HAECC4;
when "00"&std_logic_vector(to_unsigned(236,8)) => o_data <= "1"&x"d8";
when "00"&std_logic_vector(to_unsigned(237,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(238,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(239,8)) => o_data <= "1"&REG_HAECC5;
when "00"&std_logic_vector(to_unsigned(240,8)) => o_data <= "1"&x"f0";
when "00"&std_logic_vector(to_unsigned(241,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(242,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(243,8)) => o_data <= "1"&REG_HAECC6;
when "00"&std_logic_vector(to_unsigned(244,8)) => o_data <= "1"&x"90";
when "00"&std_logic_vector(to_unsigned(245,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(246,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(247,8)) => o_data <= "1"&REG_HAECC7;
when "00"&std_logic_vector(to_unsigned(248,8)) => o_data <= "1"&x"94";
when "00"&std_logic_vector(to_unsigned(249,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(250,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(251,8)) => o_data <= "1"&REG_COM8;
when "00"&std_logic_vector(to_unsigned(252,8)) => o_data <= "1"&(COM8_FASTAEC or COM8_AECSTEP or COM8_BFILT or COM8_AGC or COM8_AEC);
when "00"&std_logic_vector(to_unsigned(253,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(254,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(255,8)) => o_data <= "1"&REG_COM5;
when "00"&std_logic_vector(to_unsigned(256,8)) => o_data <= "1"&x"61";
when "00"&std_logic_vector(to_unsigned(257,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(258,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(259,8)) => o_data <= "1"&REG_COM6;
when "00"&std_logic_vector(to_unsigned(260,8)) => o_data <= "1"&x"4b";
when "00"&std_logic_vector(to_unsigned(261,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(262,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(263,8)) => o_data <= "1"&x"16";
when "00"&std_logic_vector(to_unsigned(264,8)) => o_data <= "1"&x"02";
when "00"&std_logic_vector(to_unsigned(265,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(266,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(267,8)) => o_data <= "1"&REG_MVFP;
when "00"&std_logic_vector(to_unsigned(268,8)) => o_data <= "1"&x"07";
when "00"&std_logic_vector(to_unsigned(269,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(270,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(271,8)) => o_data <= "1"&x"21";
when "00"&std_logic_vector(to_unsigned(272,8)) => o_data <= "1"&x"02";
when "00"&std_logic_vector(to_unsigned(273,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(274,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(275,8)) => o_data <= "1"&x"22";
when "00"&std_logic_vector(to_unsigned(276,8)) => o_data <= "1"&x"91";
when "00"&std_logic_vector(to_unsigned(277,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(278,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(279,8)) => o_data <= "1"&x"29";
when "00"&std_logic_vector(to_unsigned(280,8)) => o_data <= "1"&x"07";
when "00"&std_logic_vector(to_unsigned(281,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(282,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(283,8)) => o_data <= "1"&x"33";
when "00"&std_logic_vector(to_unsigned(284,8)) => o_data <= "1"&x"0b";
when "00"&std_logic_vector(to_unsigned(285,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(286,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(287,8)) => o_data <= "1"&x"35";
when "00"&std_logic_vector(to_unsigned(288,8)) => o_data <= "1"&x"0b";
when "00"&std_logic_vector(to_unsigned(289,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(290,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(291,8)) => o_data <= "1"&x"37";
when "00"&std_logic_vector(to_unsigned(292,8)) => o_data <= "1"&x"1b";
when "00"&std_logic_vector(to_unsigned(293,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(294,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(295,8)) => o_data <= "1"&x"38";
when "00"&std_logic_vector(to_unsigned(296,8)) => o_data <= "1"&x"71";
when "00"&std_logic_vector(to_unsigned(297,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(298,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(299,8)) => o_data <= "1"&x"39";
when "00"&std_logic_vector(to_unsigned(300,8)) => o_data <= "1"&x"2a";
when "00"&std_logic_vector(to_unsigned(301,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(302,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(303,8)) => o_data <= "1"&REG_COM12;
when "00"&std_logic_vector(to_unsigned(304,8)) => o_data <= "1"&x"78";
when "00"&std_logic_vector(to_unsigned(305,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(306,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(307,8)) => o_data <= "1"&x"4d";
when "00"&std_logic_vector(to_unsigned(308,8)) => o_data <= "1"&x"40";
when "00"&std_logic_vector(to_unsigned(309,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(310,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(311,8)) => o_data <= "1"&x"4e";
when "00"&std_logic_vector(to_unsigned(312,8)) => o_data <= "1"&x"20";
when "00"&std_logic_vector(to_unsigned(313,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(314,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(315,8)) => o_data <= "1"&REG_GFIX;
when "00"&std_logic_vector(to_unsigned(316,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(317,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(318,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(319,8)) => o_data <= "1"&x"6b";
when "00"&std_logic_vector(to_unsigned(320,8)) => o_data <= "1"&x"0a";
when "00"&std_logic_vector(to_unsigned(321,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(322,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(323,8)) => o_data <= "1"&x"74";
when "00"&std_logic_vector(to_unsigned(324,8)) => o_data <= "1"&x"10";
when "00"&std_logic_vector(to_unsigned(325,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(326,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(327,8)) => o_data <= "1"&x"8d";
when "00"&std_logic_vector(to_unsigned(328,8)) => o_data <= "1"&x"4f";
when "00"&std_logic_vector(to_unsigned(329,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(330,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(331,8)) => o_data <= "1"&x"8e";
when "00"&std_logic_vector(to_unsigned(332,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(333,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(334,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(335,8)) => o_data <= "1"&x"8f";
when "00"&std_logic_vector(to_unsigned(336,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(337,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(338,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(339,8)) => o_data <= "1"&x"90";
when "00"&std_logic_vector(to_unsigned(340,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(341,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(342,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(343,8)) => o_data <= "1"&x"91";
when "00"&std_logic_vector(to_unsigned(344,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(345,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(346,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(347,8)) => o_data <= "1"&x"96";
when "00"&std_logic_vector(to_unsigned(348,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(349,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(350,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(351,8)) => o_data <= "1"&x"9a";
when "00"&std_logic_vector(to_unsigned(352,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(353,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(354,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(355,8)) => o_data <= "1"&x"b0";
when "00"&std_logic_vector(to_unsigned(356,8)) => o_data <= "1"&x"84";
when "00"&std_logic_vector(to_unsigned(357,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(358,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(359,8)) => o_data <= "1"&x"b1";
when "00"&std_logic_vector(to_unsigned(360,8)) => o_data <= "1"&x"0c";
when "00"&std_logic_vector(to_unsigned(361,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(362,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(363,8)) => o_data <= "1"&x"b2";
when "00"&std_logic_vector(to_unsigned(364,8)) => o_data <= "1"&x"0e";
when "00"&std_logic_vector(to_unsigned(365,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(366,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(367,8)) => o_data <= "1"&x"b3";
when "00"&std_logic_vector(to_unsigned(368,8)) => o_data <= "1"&x"82";
when "00"&std_logic_vector(to_unsigned(369,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(370,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(371,8)) => o_data <= "1"&x"b8";
when "00"&std_logic_vector(to_unsigned(372,8)) => o_data <= "1"&x"0a";
when "00"&std_logic_vector(to_unsigned(373,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(374,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(375,8)) => o_data <= "1"&x"43";
when "00"&std_logic_vector(to_unsigned(376,8)) => o_data <= "1"&x"0a";
when "00"&std_logic_vector(to_unsigned(377,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(378,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(379,8)) => o_data <= "1"&x"44";
when "00"&std_logic_vector(to_unsigned(380,8)) => o_data <= "1"&x"0f";
when "00"&std_logic_vector(to_unsigned(381,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(382,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(383,8)) => o_data <= "1"&x"45";
when "00"&std_logic_vector(to_unsigned(384,8)) => o_data <= "1"&x"34";
when "00"&std_logic_vector(to_unsigned(385,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(386,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(387,8)) => o_data <= "1"&x"46";
when "00"&std_logic_vector(to_unsigned(388,8)) => o_data <= "1"&x"58";
when "00"&std_logic_vector(to_unsigned(389,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(390,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(391,8)) => o_data <= "1"&x"47";
when "00"&std_logic_vector(to_unsigned(392,8)) => o_data <= "1"&x"28";
when "00"&std_logic_vector(to_unsigned(393,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(394,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(395,8)) => o_data <= "1"&x"48";
when "00"&std_logic_vector(to_unsigned(396,8)) => o_data <= "1"&x"3a";
when "00"&std_logic_vector(to_unsigned(397,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(398,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(399,8)) => o_data <= "1"&x"59";
when "00"&std_logic_vector(to_unsigned(400,8)) => o_data <= "1"&x"88";
when "00"&std_logic_vector(to_unsigned(401,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(402,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(403,8)) => o_data <= "1"&x"5a";
when "00"&std_logic_vector(to_unsigned(404,8)) => o_data <= "1"&x"88";
when "00"&std_logic_vector(to_unsigned(405,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(406,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(407,8)) => o_data <= "1"&x"5b";
when "00"&std_logic_vector(to_unsigned(408,8)) => o_data <= "1"&x"44";
when "00"&std_logic_vector(to_unsigned(409,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(410,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(411,8)) => o_data <= "1"&x"5c";
when "00"&std_logic_vector(to_unsigned(412,8)) => o_data <= "1"&x"67";
when "00"&std_logic_vector(to_unsigned(413,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(414,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(415,8)) => o_data <= "1"&x"5d";
when "00"&std_logic_vector(to_unsigned(416,8)) => o_data <= "1"&x"49";
when "00"&std_logic_vector(to_unsigned(417,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(418,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(419,8)) => o_data <= "1"&x"5e";
when "00"&std_logic_vector(to_unsigned(420,8)) => o_data <= "1"&x"0e";
when "00"&std_logic_vector(to_unsigned(421,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(422,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(423,8)) => o_data <= "1"&x"6c";
when "00"&std_logic_vector(to_unsigned(424,8)) => o_data <= "1"&x"0a";
when "00"&std_logic_vector(to_unsigned(425,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(426,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(427,8)) => o_data <= "1"&x"6d";
when "00"&std_logic_vector(to_unsigned(428,8)) => o_data <= "1"&x"55";
when "00"&std_logic_vector(to_unsigned(429,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(430,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(431,8)) => o_data <= "1"&x"6e";
when "00"&std_logic_vector(to_unsigned(432,8)) => o_data <= "1"&x"11";
when "00"&std_logic_vector(to_unsigned(433,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(434,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(435,8)) => o_data <= "1"&x"6f";
when "00"&std_logic_vector(to_unsigned(436,8)) => o_data <= "1"&x"9f";
when "00"&std_logic_vector(to_unsigned(437,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(438,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(439,8)) => o_data <= "1"&x"6a";
when "00"&std_logic_vector(to_unsigned(440,8)) => o_data <= "1"&x"40";
when "00"&std_logic_vector(to_unsigned(441,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(442,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(443,8)) => o_data <= "1"&REG_BLUE;
when "00"&std_logic_vector(to_unsigned(444,8)) => o_data <= "1"&x"40";
when "00"&std_logic_vector(to_unsigned(445,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(446,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(447,8)) => o_data <= "1"&REG_RED;
when "00"&std_logic_vector(to_unsigned(448,8)) => o_data <= "1"&x"60";
when "00"&std_logic_vector(to_unsigned(449,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(450,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(451,8)) => o_data <= "1"&REG_COM8;
when "00"&std_logic_vector(to_unsigned(452,8)) => o_data <= "1"&(COM8_FASTAEC or COM8_AECSTEP or COM8_BFILT or COM8_AGC or COM8_AEC or COM8_AWB);
when "00"&std_logic_vector(to_unsigned(453,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(454,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(455,8)) => o_data <= "1"&REG_CMATRIX_1;
when "00"&std_logic_vector(to_unsigned(456,8)) => o_data <= "1"&x"80";
when "00"&std_logic_vector(to_unsigned(457,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(458,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(459,8)) => o_data <= "1"&REG_CMATRIX_2;
when "00"&std_logic_vector(to_unsigned(460,8)) => o_data <= "1"&x"80";
when "00"&std_logic_vector(to_unsigned(461,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(462,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(463,8)) => o_data <= "1"&REG_CMATRIX_3;
when "00"&std_logic_vector(to_unsigned(464,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(465,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(466,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(467,8)) => o_data <= "1"&REG_CMATRIX_4;
when "00"&std_logic_vector(to_unsigned(468,8)) => o_data <= "1"&x"22";
when "00"&std_logic_vector(to_unsigned(469,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(470,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(471,8)) => o_data <= "1"&REG_CMATRIX_5;
when "00"&std_logic_vector(to_unsigned(472,8)) => o_data <= "1"&x"5e";
when "00"&std_logic_vector(to_unsigned(473,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(474,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(475,8)) => o_data <= "1"&REG_CMATRIX_6;
when "00"&std_logic_vector(to_unsigned(476,8)) => o_data <= "1"&x"80";
when "00"&std_logic_vector(to_unsigned(477,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(478,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(479,8)) => o_data <= "1"&REG_CMATRIX_SIGN;
when "00"&std_logic_vector(to_unsigned(480,8)) => o_data <= "1"&x"9e";
when "00"&std_logic_vector(to_unsigned(481,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(482,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(483,8)) => o_data <= "1"&REG_COM16;
when "00"&std_logic_vector(to_unsigned(484,8)) => o_data <= "1"&COM16_AWBGAIN;
when "00"&std_logic_vector(to_unsigned(485,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(486,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(487,8)) => o_data <= "1"&REG_EDGE;
when "00"&std_logic_vector(to_unsigned(488,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(489,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(490,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(491,8)) => o_data <= "1"&x"75";
when "00"&std_logic_vector(to_unsigned(492,8)) => o_data <= "1"&x"05";
when "00"&std_logic_vector(to_unsigned(493,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(494,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(495,8)) => o_data <= "1"&x"76";
when "00"&std_logic_vector(to_unsigned(496,8)) => o_data <= "1"&x"e1";
when "00"&std_logic_vector(to_unsigned(497,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(498,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(499,8)) => o_data <= "1"&REG_DENOISE_STRENGTH;
when "00"&std_logic_vector(to_unsigned(500,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(501,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(502,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(503,8)) => o_data <= "1"&x"77";
when "00"&std_logic_vector(to_unsigned(504,8)) => o_data <= "1"&x"01";
when "00"&std_logic_vector(to_unsigned(505,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(506,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(507,8)) => o_data <= "1"&x"4b";
when "00"&std_logic_vector(to_unsigned(508,8)) => o_data <= "1"&x"09";
when "00"&std_logic_vector(to_unsigned(509,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(510,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(511,8)) => o_data <= "1"&x"c9";
when "00"&std_logic_vector(to_unsigned(512,8)) => o_data <= "1"&x"60";
when "00"&std_logic_vector(to_unsigned(513,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(514,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(515,8)) => o_data <= "1"&REG_CONTRAST;
when "00"&std_logic_vector(to_unsigned(516,8)) => o_data <= "1"&x"40";
when "00"&std_logic_vector(to_unsigned(517,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(518,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(519,8)) => o_data <= "1"&x"34";
when "00"&std_logic_vector(to_unsigned(520,8)) => o_data <= "1"&x"11";
when "00"&std_logic_vector(to_unsigned(521,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(522,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(523,8)) => o_data <= "1"&REG_COM11;
when "00"&std_logic_vector(to_unsigned(524,8)) => o_data <= "1"&(COM11_EXP or COM11_HZAUTO);
when "00"&std_logic_vector(to_unsigned(525,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(526,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(527,8)) => o_data <= "1"&x"a4";
when "00"&std_logic_vector(to_unsigned(528,8)) => o_data <= "1"&x"88";
when "00"&std_logic_vector(to_unsigned(529,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(530,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(531,8)) => o_data <= "1"&x"96";
when "00"&std_logic_vector(to_unsigned(532,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(533,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(534,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(535,8)) => o_data <= "1"&x"97";
when "00"&std_logic_vector(to_unsigned(536,8)) => o_data <= "1"&x"30";
when "00"&std_logic_vector(to_unsigned(537,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(538,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(539,8)) => o_data <= "1"&x"98";
when "00"&std_logic_vector(to_unsigned(540,8)) => o_data <= "1"&x"20";
when "00"&std_logic_vector(to_unsigned(541,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(542,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(543,8)) => o_data <= "1"&x"99";
when "00"&std_logic_vector(to_unsigned(544,8)) => o_data <= "1"&x"30";
when "00"&std_logic_vector(to_unsigned(545,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(546,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(547,8)) => o_data <= "1"&x"9a";
when "00"&std_logic_vector(to_unsigned(548,8)) => o_data <= "1"&x"84";
when "00"&std_logic_vector(to_unsigned(549,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(550,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(551,8)) => o_data <= "1"&x"9b";
when "00"&std_logic_vector(to_unsigned(552,8)) => o_data <= "1"&x"29";
when "00"&std_logic_vector(to_unsigned(553,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(554,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(555,8)) => o_data <= "1"&x"9c";
when "00"&std_logic_vector(to_unsigned(556,8)) => o_data <= "1"&x"03";
when "00"&std_logic_vector(to_unsigned(557,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(558,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(559,8)) => o_data <= "1"&x"9d";
when "00"&std_logic_vector(to_unsigned(560,8)) => o_data <= "1"&x"4c";
when "00"&std_logic_vector(to_unsigned(561,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(562,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(563,8)) => o_data <= "1"&x"9e";
when "00"&std_logic_vector(to_unsigned(564,8)) => o_data <= "1"&x"3f";
when "00"&std_logic_vector(to_unsigned(565,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(566,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(567,8)) => o_data <= "1"&x"78";
when "00"&std_logic_vector(to_unsigned(568,8)) => o_data <= "1"&x"04";
when "00"&std_logic_vector(to_unsigned(569,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(570,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(571,8)) => o_data <= "1"&x"79";
when "00"&std_logic_vector(to_unsigned(572,8)) => o_data <= "1"&x"01";
when "00"&std_logic_vector(to_unsigned(573,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(574,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(575,8)) => o_data <= "1"&x"c8";
when "00"&std_logic_vector(to_unsigned(576,8)) => o_data <= "1"&x"0f";
when "00"&std_logic_vector(to_unsigned(577,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(578,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(579,8)) => o_data <= "1"&x"79";
when "00"&std_logic_vector(to_unsigned(580,8)) => o_data <= "1"&x"0f";
when "00"&std_logic_vector(to_unsigned(581,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(582,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(583,8)) => o_data <= "1"&x"c8";
when "00"&std_logic_vector(to_unsigned(584,8)) => o_data <= "1"&x"00";
when "00"&std_logic_vector(to_unsigned(585,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(586,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(587,8)) => o_data <= "1"&x"79";
when "00"&std_logic_vector(to_unsigned(588,8)) => o_data <= "1"&x"10";
when "00"&std_logic_vector(to_unsigned(589,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(590,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(591,8)) => o_data <= "1"&x"c8";
when "00"&std_logic_vector(to_unsigned(592,8)) => o_data <= "1"&x"7e";
when "00"&std_logic_vector(to_unsigned(593,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(594,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(595,8)) => o_data <= "1"&x"79";
when "00"&std_logic_vector(to_unsigned(596,8)) => o_data <= "1"&x"0a";
when "00"&std_logic_vector(to_unsigned(697,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(598,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(599,8)) => o_data <= "1"&x"c8";
when "00"&std_logic_vector(to_unsigned(600,8)) => o_data <= "1"&x"80";
when "00"&std_logic_vector(to_unsigned(601,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(602,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(603,8)) => o_data <= "1"&x"79";
when "00"&std_logic_vector(to_unsigned(604,8)) => o_data <= "1"&x"0b";
when "00"&std_logic_vector(to_unsigned(605,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(606,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(607,8)) => o_data <= "1"&x"c8";
when "00"&std_logic_vector(to_unsigned(608,8)) => o_data <= "1"&x"01";
when "00"&std_logic_vector(to_unsigned(609,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(610,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(611,8)) => o_data <= "1"&x"79";
when "00"&std_logic_vector(to_unsigned(612,8)) => o_data <= "1"&x"0c";
when "00"&std_logic_vector(to_unsigned(613,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(614,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(615,8)) => o_data <= "1"&x"c8";
when "00"&std_logic_vector(to_unsigned(616,8)) => o_data <= "1"&x"0f";
when "00"&std_logic_vector(to_unsigned(617,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(618,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(619,8)) => o_data <= "1"&x"79";
when "00"&std_logic_vector(to_unsigned(620,8)) => o_data <= "1"&x"0d";
when "00"&std_logic_vector(to_unsigned(621,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(622,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(623,8)) => o_data <= "1"&x"c8";
when "00"&std_logic_vector(to_unsigned(624,8)) => o_data <= "1"&x"20";
when "00"&std_logic_vector(to_unsigned(625,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(626,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(627,8)) => o_data <= "1"&x"79";
when "00"&std_logic_vector(to_unsigned(628,8)) => o_data <= "1"&x"09";
when "00"&std_logic_vector(to_unsigned(629,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(630,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(631,8)) => o_data <= "1"&x"c8";
when "00"&std_logic_vector(to_unsigned(632,8)) => o_data <= "1"&x"80";
when "00"&std_logic_vector(to_unsigned(633,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(634,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(635,8)) => o_data <= "1"&x"79";
when "00"&std_logic_vector(to_unsigned(636,8)) => o_data <= "1"&x"02";
when "00"&std_logic_vector(to_unsigned(637,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(638,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(639,8)) => o_data <= "1"&x"c8";
when "00"&std_logic_vector(to_unsigned(640,8)) => o_data <= "1"&x"0c";
when "00"&std_logic_vector(to_unsigned(641,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(642,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(643,8)) => o_data <= "1"&x"79";
when "00"&std_logic_vector(to_unsigned(644,8)) => o_data <= "1"&x"03";
when "00"&std_logic_vector(to_unsigned(645,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(646,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(647,8)) => o_data <= "1"&x"c8";
when "00"&std_logic_vector(to_unsigned(648,8)) => o_data <= "1"&x"40";
when "00"&std_logic_vector(to_unsigned(649,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(650,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(651,8)) => o_data <= "1"&x"79";
when "00"&std_logic_vector(to_unsigned(652,8)) => o_data <= "1"&x"05";
when "00"&std_logic_vector(to_unsigned(653,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(654,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(655,8)) => o_data <= "1"&x"c8";
when "00"&std_logic_vector(to_unsigned(656,8)) => o_data <= "1"&x"30";
when "00"&std_logic_vector(to_unsigned(657,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(658,8)) => o_data <= "1"&x"42";
when "00"&std_logic_vector(to_unsigned(659,8)) => o_data <= "1"&x"79";
when "00"&std_logic_vector(to_unsigned(660,8)) => o_data <= "1"&x"26";
when "00"&std_logic_vector(to_unsigned(661,8)) => o_data <= "011111111";

when "00"&std_logic_vector(to_unsigned(662,8)) => o_data <= "0"&x"fe";
when "00"&std_logic_vector(to_unsigned(663,8)) => o_data <= "0"&x"fe";
when "00"&std_logic_vector(to_unsigned(664,8)) => o_data <= "0"&x"fe";
when "00"&std_logic_vector(to_unsigned(665,8)) => o_data <= "0"&x"fe";
when "00"&std_logic_vector(to_unsigned(666,8)) => o_data <= "0"&x"fe";
when "00"&std_logic_vector(to_unsigned(667,8)) => o_data <= "0"&x"fe";
when "00"&std_logic_vector(to_unsigned(668,8)) => o_data <= "0"&x"fe";
when "00"&std_logic_vector(to_unsigned(669,8)) => o_data <= "0"&x"fe";
when "00"&std_logic_vector(to_unsigned(670,8)) => o_data <= "0"&x"fe";
when "00"&std_logic_vector(to_unsigned(671,8)) => o_data <= "0"&x"fe";
when "00"&std_logic_vector(to_unsigned(672,8)) => o_data <= "0"&x"fe";
when "00"&std_logic_vector(to_unsigned(673,8)) => o_data <= "001010100";

when others => o_data <= (others =>'0');

end case;
end if;
end process;
end architecture Behavioral;
